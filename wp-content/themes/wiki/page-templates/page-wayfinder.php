<?php
/*
Template Name: Wayfinder Page
*/

get_layout('header', 'sidebar');
get_layout('search', 'form');
get_layout('wayfinder', 'child');
get_layout('footer', 'sidebar');
