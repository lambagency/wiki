<?php
/*
Template Name: Sitemap Page
*/

get_layout('header', 'sidebar');
get_layout('sitemap');
get_layout('footer', 'sidebar');
