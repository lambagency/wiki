<?php
/*
Template Name: Module Page
*/

get_layout('header', 'sidebar');
get_layout('modules');
get_layout('footer', 'sidebar');
