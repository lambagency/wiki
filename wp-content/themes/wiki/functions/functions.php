<?php

require_once ABSPATH . 'wp-admin/includes/plugin.php';


/**
 * Ensure 'Lamb Functions' plugin is activated
 */
function activateLambPlugins()
{
    $plugins = [
        'lamb-functions',
        'lamb-acf-button',
        'lamb-acf-title-block'
    ];

    foreach ($plugins as $plugin) {
        if (!is_plugin_active($plugin)) {
            activate_plugin(WP_PLUGIN_DIR . '/' . $plugin . '/' . $plugin . '.php');
        }
    }
}
add_action('init', 'activateLambPlugins');


/**
 * Include any custom function files here.
 * If you have suggestions for adding new features to the 'Lamb Functions' plugin, please raise as a team.
 */


require_once('config.php');
require_once('login.php');
require_once('theme.php');
