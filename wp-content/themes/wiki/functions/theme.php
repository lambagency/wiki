<?php

/**
 * Registers custom scripts used throughout the site's templates
 */
function registerAssets()
{
    wp_enqueue_style(
        'google-fonts',
        '//fonts.googleapis.com/css?family=Source+Sans+Pro:200,400,700,400italic'
    );

    if (($_SERVER['SERVER_NAME'] == 'local') && defined('LIVERELOAD_ENABLED') && LIVERELOAD_ENABLED) {
        wp_enqueue_style(
            'production-css',
            'http://localhost:8000/build/' . ((defined('DEV_ENVIRONMENT') && DEV_ENVIRONMENT) ? 'production.css' : 'production.min.css')
        );

        wp_enqueue_script(
            'livereload-js',
            '//localhost:35729/livereload.js',
            array(),
            false,
            true
        );
    } else {
        wp_enqueue_style(
            'production-css',
            BUILD_URI . ((defined('DEV_ENVIRONMENT') && DEV_ENVIRONMENT) ? 'production.css' : 'production.min.css')
        );
    }

    wp_deregister_script('jquery');
    wp_enqueue_script(
        'jquery',
        '//ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js',
        array(),
        false,
        true
    );

    wp_enqueue_script(
        'production-js',
        BUILD_URI . ((defined('DEV_ENVIRONMENT') && DEV_ENVIRONMENT) ? 'production.js' : 'production.min.js'),
        array('jquery'),
        false,
        true
    );

    wp_localize_script(
        'production-js',
        'productionJS',
        array(
            'baseUrl'  => BASE_URL,
            'themeUrl' => THEME_URI,
            'jsonUrl'  => JSON_URL,
            'ajaxUrl'  => AJAX_URL
        )
    );
}

add_action('wp_enqueue_scripts', 'registerAssets');


/**
 * Add IE Conditional scripts for Modenizr and Respond.js
 */
//function ieConditionalScripts()
//{
//    echo '
//        <!--[if IE 8|IE 9]>
//            <script src="' . BUILD_URI . 'js/' . ((defined('DEV_ENVIRONMENT') && DEV_ENVIRONMENT) ? 'modernizr.js' : 'modernizr.min.js') . '"></script>
//        <![endif]-->
//        <!--[if IE 8]>
//            <script src="' . BUILD_URI . 'js/respond.js"></script>
//        <![endif]-->
//    ';
//}
//
//add_action('wp_head', 'ieConditionalScripts');