/**
 * CONFIG
 * Global config for theme
 */
window.themeConfig = {

    /**
     * Breakpoints must match breakpoints use in SCSS (_variables.scss)
     */
    breakpoints: {
        mobileMid: 550,
        tabletPortrait: 768,
        tabletLandscape: 1024
    },

    /**
     * Check if mobile browser, based on user agent string
     */
    isMobile:  {

        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (themeConfig.isMobile.Android() || themeConfig.isMobile.BlackBerry() || themeConfig.isMobile.iOS() ||
                    themeConfig.isMobile.Opera() || themeConfig.isMobile.Windows());
        }
    },

    /**
     * Check if desktop browser, based on user agent string
     */
    isDesktop: function() {
        return themeConfig.isMobile.any() === null
    },

    /**
     * Holds variables which check for browser support of elements, methods, etc
     */
    support: {
        html5Video: (!!document.createElement('video').canPlayType)
    }

};