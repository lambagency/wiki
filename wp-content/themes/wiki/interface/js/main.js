
(function( $, themeConfig, device, lazyImages ){
    'use strict';

    jQuery(document).ready(function() {
        _main.initBase();
        _main.initIcons();
        _main.initAllModulesComponents();
        _main.wpadminbar();
        _main.initModernizrFunctions();
        _main.initAnimations();
        _main.initLazyImages();
        _main.initCheckHashInURL();
        _main.initScrollTo();
    });

    var _main = {
        initBase: function() {

            var header = $('header'),
                footer = $('footer');

            // Init Header
            header.modheader();
        },


        initIcons: function() {

            // Converts SVG external links to inline html, this allows for complete customisation via css
            // IE 9-11 rely on this as they do not support SVG external links
            svg4everybody({
                polyfill: true
            });
        },
        
        initAllModulesComponents : function() {
            _main.initModCompFuncs( $('.module') );
            _main.initModCompFuncs( $('.component') );
        },


        //TODO: see if this is fine, or alternatively, a better implementation
        initModCompFuncs : function(element) {

            if ( !!element ) {
                // Loop through all modules and initialise
                element.each(function() {
                    var elementNameArray = element.attr('class').split(' ', 2),
                        elementName      = elementNameArray[0],
                        elementID        = $(this).data(elementName);

                    // Check if module/component has module/component ID defined (data-module="")
                    if (typeof elementID !== 'undefined') {
                        var elementFuncName;

                        if ( elementName === 'module') {
                            elementFuncName  = 'mod' + elementID;
                        } else if ( elementName === 'component') {
                            elementFuncName  = elementName + elementID;
                        }

                        // Check if function for module exists
                        if ( $.isFunction($.fn[elementFuncName]) ) {
                            $(this)[elementFuncName]();
                        }
                    }
                });
            }

        },

        wpadminbar: function()
        {
            if($('#wpadminbar').length) {
                $('header').addClass('wpadminbar');
            }
        },


        initModernizrFunctions: function()
        {
            // add placeholder for IE9 and below
            if (typeof Modernizr == 'object') {
                if (Modernizr && !Modernizr.input.placeholder) {
                    $('[placeholder]').focus(function () {
                        var input = $(this);
                        if (input.val() === input.attr('placeholder')) {
                            input.val('');
                            input.removeClass('placeholder');
                        }
                    }).blur(function () {
                        var input = $(this);
                        if (input.val() === '' || input.val() === input.attr('placeholder')) {
                            input.addClass('placeholder');
                            input.val(input.attr('placeholder'));
                        }
                    }).blur();

                    $('[placeholder]').parents('form').submit(function () {
                        $(this).find('[placeholder]').each(function () {
                            var input = $(this);
                            if (input.val() === input.attr('placeholder')) {
                                input.val('');
                            }
                        });
                    });
                }
            }
        },

        initAnimations: function() {

            if (themeConfig.isDesktop()) {

                var animatedElements = $('.animated').not('[data-animate-hold]');

                animatedElements.appear({
                    force_process: true // Force on load
                });

                // Animate on element appear
                animatedElements.on('appear', function() {
                    $(this).customAnimate();
                });

                // Animate on element disappear
                animatedElements.on('disappear', function() {
                    if (typeof $(this).data('animate-repeat') !== 'undefined') {
                        $(this).customAnimate('hide');
                    }
                });

                // Force appear to run if device state change
                device.on('stateChange', function() {
                    $.force_appear();
                });
            }
            else {
                $('.animated').css('visibility', 'visible');
            }
        },

        initLazyImages: function() {

            lazyImages.init();

            // Force lazy images refresh if device state change
            device.on('stateChange', function() {
                lazyImages.refresh();
            });
        },

        initCheckHashInURL: function()
        {
            if(window.location.hash) {
                var hash    = window.location.hash.substring(1),
                    target  = $('#' + hash);

                if (target.length) {

                    var scrollOffset = _main.initScrollOffset();

                    scrollTo(target, 400, scrollOffset);
                }
            }
        },


        initScrollTo: function()
        {
            // default scroll animation time (ms)
            // Use data-scroll-duration="" to override
            var scrollDurationDefault = 400;

            $('.scroll-to').click(function(e) {
                e.preventDefault();

                var ele = $(this);

                // check for scroll position data attribute
                var scrollToEle = ele.data('scroll-to') !== undefined ? ele.data('scroll-to') : false;
                scrollToEle     = scrollToEle ? $('.' + scrollToEle).first() : false;

                // Target
                var target      = scrollToEle ? scrollToEle : this.hash;

                // Duration
                var duration = ele.data('scroll-duration');
                duration = (duration === undefined) ? scrollDurationDefault : duration;

                var scrollOffset = _main.initScrollOffset();

                scrollTo(target, duration, scrollOffset);
            });
        },


        initScrollOffset: function()
        {
            var wpadminbar      = $('#wpadminbar').height(),
                header          = $('header'), // header - used to define offset height
                headerFixed     = header.css('position') === 'fixed';

            // Offset
            var scrollOffset    = 0;

            scrollOffset += wpadminbar;

            if (headerFixed) {
                scrollOffset += header.height();
            }

            return scrollOffset;
        }
    };

}( jQuery, themeConfig, device, lazyImages ));