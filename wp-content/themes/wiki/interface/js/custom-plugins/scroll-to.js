/**
 * ======================================================================
 * SCROLL TO
 * ======================================================================
 * Smooth scroll to element
 *
 * Authors: Carlton - LambAgency
 * ======================================================================
 */
(function() {

    /**
     * Smooth scroll to a position on the page
     * @param element - The Element to scroll to
     * @param time - The duration of the scroll
     * @param offset - The scroll offset
     */
    scrollTo = function(element, time, offset){

        if ($(element).length) {
            var scroll_position = $(element).offset().top;
            scroll_position = scroll_position - offset;
            scroll_position = scroll_position > 0 ? scroll_position : 0;

            $('html, body').animate({
                scrollTop: scroll_position
            }, time)
        }
    };
})(jQuery);