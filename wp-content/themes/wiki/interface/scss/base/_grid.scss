//===============================================================//
// Grid Framework
//===============================================================//
//
// Originated from toast grid framework. Modified by Lamb Agency
// https://github.com/daneden/toast
//
// Creates a responsive inline block grid. Allows for custom column
// number generation for all breakpoints.
//
//===============================================================//
// Usage
//===============================================================//
//
//  PARAMS:
//      $grid-namespace         {String}- The grid row css namespace
//      $grid-column-namespace  {String}- The grid column css namespace
//      $col-groups             {Integer Array}- The columns to generate for tablet portait and above
//      $col-groups-small       {Integer Array}- The columns to generate for mobile
//      $col-groups-medium      {Integer Array}- The columns to generate for tablet portrait
//      $col-groups-large       {Integer Array}- The columns to generate for tablet landscape
//      $column-margin          {Pixel Value|Percentage}- Default column margin
//
//  HTML:
//
//      STANDARD USAGE:
//
//          <div class="row">
//              <div class="col col-1-2"></div>
//              <div class="col col-1-4"></div>
//              <div class="col col-1-4"></div>
//          </div>
//
//
//      ADVANCED USAGE:
//
//      This following html will generate columns of the following structure:
//          @ Mobile: 2 columns
//          @ Mobile - Tablet Portrait: 4 columns
//          @ Tablet Portrait - Tablet Landscape: 8 columns
//          @ Desktop: 10 columns
//
//      <div class="row">
//          <div class="col col-s-1-2 col-m-1-4 col-l-1-8 col-1-10"></div>
//          <div class="col col-s-1-2 col-m-1-4 col-l-1-8 col-1-10"></div>
//      </div>
//
//
//  ROW CLASSES:
//
//      .grid-no-gutter | Removes the padding for the child columns
//
//      .grid-no-side-gutter | Adds a negative margin to the row container
//                             to remove the padding on the first and last
//                             column element.
//
//      .grid-am | Vertically align all child columns to the middle of the row
//
//      .grid-ab | Vertically align all child columns to the bottom of the row
//
//
//  COLUMN CLASSES:
//
//      .col-centered | Centers column and clears row of the rest of columns
//
//      .col-d-first  | Floats column to appear as first in row
//
//      .col-d-last   | Floats column to appear as last in row
//
//      .col-am       | Vertically aligns column to the middle of the row
//
//      .col-ab       | Vertically aligns column to the bottom of the row
//
//===============================================================//


//===============================================================//
// Variables
//===============================================================//

$grid-namespace: "row";         // Row CSS Namespace
$grid-column-namespace: "col";  // Column CSS Namespace
$col-groups: (1, 2, 3, 4, 8);   // Tablet Portrait <
$col-groups-small: (1);         // Mobile
$col-groups-medium: (2);        // Tablet Portrait
$col-groups-large: (1, 2);      // Tablet Landscape
$column-margin: 15px;           // Column Margins


//===============================================================//
// Grid Generation
//===============================================================//

.#{$grid-namespace} {
    list-style: none;

    // For each of our column groups...
    @each $group in $col-groups {

        // For each column width from 1 to the column group...
        @for $i from 1 through $group {
            .#{$grid-column-namespace}-#{$i}-#{$group} {
                width: percentage($i/$group);
            }

            // Push Classes
            .#{$grid-column-namespace}-push-#{$i}-#{$group} {
                margin-left: percentage($i/$group);
            }

            // Pull Classes
            .#{$grid-column-namespace}-pull-#{$i}-#{$group} {
                margin-left: -(percentage($i/$group));
            }
        } // end @for

    } // end @each



    .#{$grid-column-namespace} {
        display: inline-block;
        margin-right: -.26em;
        min-height: 1px;
        padding-left: $column-margin;
        padding-right: $column-margin;
        vertical-align: top;

        @include box-sizing;


        // Default to single column for below Tablet Portrait
        @include mq($max, $tablet-portrait-breakpoint) {
            display: block;
            width: auto;
            margin-left: 0;
            margin-right: 0;
            margin-bottom: 20px;
            padding-left: 0;
            padding-right: 0;
        }


        //===============================================================//
        // Mobile
        //===============================================================//

        @include mq($max, $mobile-mid-breakpoint) {
            &[class*="#{$grid-column-namespace}-s-"] {
                display: inline-block;
                margin-right: -.24em;
                padding-left: $column-margin;
                padding-right: $column-margin;
                margin-bottom: 0;
            }

            // For each of our column groups...
            @each $group in $col-groups-small {

                // For each column width from 1 to the column group...
                @for $i from 1 through $group {
                    &.#{$grid-column-namespace}-s-#{$i}-#{$group} {
                        width: percentage($i/$group);
                    }
                } // end @for
            } // end @each
        }


        //===============================================================//
        // Tablet Portrait - Tablet Landscape
        //===============================================================//

        @include mq('', $mobile-mid-breakpoint, $tablet-portrait-breakpoint) {
            &[class*="#{$grid-column-namespace}-m-"] {
                display: inline-block;
                margin-right: -.24em;
                padding-left: $column-margin;
                padding-right: $column-margin;
                margin-bottom: 0;
            }

            // For each of our column groups...
            @each $group in $col-groups-medium {

                // For each column width from 1 to the column group...
                @for $i from 1 through $group {
                    &.#{$grid-column-namespace}-m-#{$i}-#{$group} {
                        width: percentage($i/$group);
                    }
                } // end @for
            } // end @each
        }


        //===============================================================//
        // Tablet Landscape - Desktop
        //===============================================================//

        @include mq('', $tablet-portrait-breakpoint, $tablet-landscape-breakpoint) {
            &[class*="#{$grid-column-namespace}-l-"] {
                display: inline-block;
                margin-right: -.24em;
                padding-left: $column-margin;
                padding-right: $column-margin;
                margin-bottom: 0;
            }

            // For each of our column groups...
            @each $group in $col-groups-large {

                // For each column width from 1 to the column group...
                @for $i from 1 through $group {
                    &.#{$grid-column-namespace}-l-#{$i}-#{$group} {
                        width: percentage($i/$group);
                    }
                } // end @for
            } // end @each
        }

    }


    //===============================================================//
    // Row Classes
    //===============================================================//

    // Removes gutters from the columns
    &.grid-no-gutter {
        >.col {
            padding-left: 0;
            padding-right: 0;
        }

        .#{$grid-column-namespace}-span-all {
            margin-left: 0;
            width: 100%;
        }
    }

    // Adds negative margin to row, removes gutters from first and last columns
    &.grid-no-side-gutter {
        margin: 0 (- $column-margin);
    }

    // Vertically aligns all child columns to the middle of the row
    &.grid-am {

        .#{$grid-column-namespace} {
            vertical-align: middle;
        }
    }

    // Vertically aligns all child columns to the bottom of the row
    &.grid-ab {

        .#{$grid-column-namespace} {
            vertical-align: bottom;
        }
    }

    //===============================================================//
    // Column Classes
    //===============================================================//

    // Centers the column in the grid and clears the row of all other columns
    .#{$grid-column-namespace}-centered {
        display: block;
        margin-left: auto;
        margin-right: auto;
    }

    // Displays the column as the first in its row
    .#{$grid-column-namespace}-d-first {
        float: left;
    }

    // Displays the column as the last in its row
    .#{$grid-column-namespace}-d-last {
        float: right;
    }

    // Align column to the middle.
    .#{$grid-column-namespace}-am {
        vertical-align: middle;
    }

    // Align column to the bottom.
    .#{$grid-column-namespace}-ab {
        vertical-align: bottom;
    }

}