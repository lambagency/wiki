"use strict";

(function( $ ){

    var moduleID = 'search-form', // Must equal (data-module)
        module,                 // Holds module element
        searchInput,
        clearSearch;

    var moduleFunctions = {

        init: function() {

            clearSearch = module.find('.clear-search');
            searchInput = module.find('.search-input');

            clearSearch.click(function() {
                searchInput.val('');
                clearSearch.removeClass('active');
            });

            searchInput.on('input', function() {

                if ($(this).val() == '') {
                    clearSearch.removeClass('active');
                }
                else {
                    clearSearch.addClass('active');
                }

            });
        }
    };


    // Initialise module
    $.fn['component' + moduleID] = function () {
        module = $(this);
        moduleFunctions.init();
    };

})( jQuery );