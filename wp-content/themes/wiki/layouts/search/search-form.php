<div class="module search-form component" data-component="search-form">
    <div class="inner">
        <input placeholder="" type="text" name="algolia-search" id="search-input" class="search-input" autocomplete="off" spellcheck="false">
        <i class="clear-search"></i>
    </div>
</div>