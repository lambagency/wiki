<?php if (have_comments()) : ?>

	<div class="mod-discussion-comments">

		<ol class="comment-list">
			<?php
			$args = array(
				'avatar_size'   => 47,
				'callback'      => 'customComment'
			);

			wp_list_comments($args);
			?>
		</ol>

	</div>

<?php endif; ?>


<?php if (is_user_logged_in()) : ?>
	<div class="mod-discussion-comments-form">
		<?php comment_form(); ?>
	</div>
<?php endif; ?>