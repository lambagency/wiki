<?php if ($items) : ?>
    <ul class="accordion-list">
        <?php foreach ($items as $key => $item) : ?>

            <?php $accordionItemClass = $key == 0 ? ' first' : ''; ?>

            <li class="accordion-item<?php echo $accordionItemClass; ?>">
                <a class="accordion-title">
                    <?php echo $item['title']; ?>
                    <span class="accordion-symbol"></span>
                </a>

                <div class="accordion-detail">
                    <?php echo $item['content']; ?>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif;