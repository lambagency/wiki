<?php if ($items) : ?>

    <div class="row grid-no-gutter">

        <div class="col col-1-3 col-l-1-1 accordion-list-wrap">

            <ul class="accordion-list">
                <?php foreach ($items as $key => $item) : ?>

                    <?php $accordionItemClass = $key == 0 ? ' first open' : ''; ?>

                    <li data-accordion-item="<?php echo $key; ?>" class="accordion-item<?php echo $accordionItemClass; ?>">
                        <a class="accordion-title">
                            <?php echo $item['title']; ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>

        </div>


        <div class="col col-2-3 col-l-1-1 accordion-content-wrap">

            <ul class="accordion-list">
                <?php foreach ($items as $key => $item) : ?>

                    <?php $accordionItemClass = $key == 0 ? ' first open' : ''; ?>

                    <li data-accordion-item="<?php echo $key; ?>" class="accordion-item<?php echo $accordionItemClass; ?>">
                        <a class="accordion-title">
                            <?php echo $item['title']; ?>
                            <span class="accordion-symbol"></span>
                        </a>

                        <div class="accordion-detail">
                            <?php echo $item['content']; ?>
                        </div>
                    </li>

                <?php endforeach; ?>
            </ul>

        </div>

    </div>

<?php endif;