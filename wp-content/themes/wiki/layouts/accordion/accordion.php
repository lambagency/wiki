<?php

$fields = LambAgency\Util\getFields();
$items = get_sub_field('items');

?>


<div data-module="accordion" class="module mod-accordion <?php echo implode(' ', $fields['moduleClass']); ?>">

    <?php include(locate_template('layouts/components/background-image/background-image.php')); ?>

    <div class="layoutwidth">

        <?php \LambAgency\Util\getTitleBlock($fields['title-block']); ?>

        <?php include(locate_template('layouts/components/content/content.php')); ?>

        <?php include(locate_template('layouts/accordion/accordion-' . $fields['type'] . '-partial.php')); ?>

        <?php \LambAgency\Util\getButton(array('buttons' => $fields['buttons'])); ?>

    </div>
</div>