(function($){
    'use strict';

    var moduleID = 'testimonials', // Must equal (data-module)
        module;                   // Holds module element

    var moduleFunctions = {

        init: function() {
            var dataSlick = module.find('.slider').data('slick'),
                slider = module.find('.slider');

            if ( !!slider && (typeof slider !== 'undefined') ) {

                slider.slick({
                    slidesToShow  : 1,
                    slidesToScroll: 1,
                    dots          : true,
                    autoplay      : true,
                    autoplaySpeed : 5000,
                    arrows        : true,
                    infinite      : false,
                    responsive    : [
                        {
                            breakpoint: 1024,
                            settings  : dataSlick ||  {
                                slidesToShow  : 1,
                                slidesToScroll: 1
                            }
                        }
                    ]
                });

            }
        }

    };


    // Initialise module
    $.fn['mod' + moduleID] = function () {
        module = $(this);
        moduleFunctions.init();
    };


}(jQuery));