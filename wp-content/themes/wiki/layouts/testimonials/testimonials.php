<?php
$fields             =  LambAgency\Util\getFields();
$testimonials       = get_sub_field('testimonials');
?>

<div data-module="testimonials"
     class="module mod-testimonials <?php echo implode(' ', $fields['moduleClass']); ?>">

    <?php include(locate_template( 'layouts/components/background-image/background-image.php' )); ?>

    <div class="layoutwidth center">

        <?php \LambAgency\Util\getTitleBlock($fields['title-block']); ?>

        <?php include(locate_template('layouts/testimonials/testimonials-' . $fields['type'] . '-partial.php')); ?>
        
        <?php \LambAgency\Util\getButton(array('buttons' => $fields['buttons'])); ?>

    </div>
</div>