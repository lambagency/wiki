<?php

// Determines if it is the lasy key, if not, then add a separator string
function noSeparatorIfLast($fields = [], $separator = '//', $ID) {

    if ( !empty($fields) ) {

        $titleFields = [];

        foreach ($fields as $key => $field) {
            $fieldVal = get_field($field, $ID);

            if ($fieldVal != '') {
                $titleFields[] = $fieldVal;
            }
        }

        echo implode('<span>' . ' ' . $separator . ' ' . '</span>', $titleFields);
    }
}
?>

<?php if($testimonials) : ?>
    <div class="slider" data-slick='{"slidesToShow": 1, "slidesToScroll": 1}'>


        <?php foreach ($testimonials as $testimonial) : ?>

            <div class="slide">

                <?php if ($testimonial->image) : $photo = wp_get_attachment_image_src($testimonial->image, 'testimonial'); ?>
                    <div class="img" data-match-height="testimonial-list">
                        <div class="inner crop-img-circle">
                            <img src="<?php echo $photo['0']; ?>"
                                 alt="<?php echo esc_html($testimonial->post_title); ?>"
                                 width="<?php echo $photo['1']; ?>"
                                 height="<?php echo $photo['2']; ?>"
                                 class="crop-img-circle" />
                        </div>
                    </div>
                <?php endif; ?>

                <div class="details<?php echo $testimonial->image ? '' : ' no-img'; ?>" data-match-height="testimonial-list">
                    <div class="vert-align">
                        <div class="inner">

                            <p class="message">
                                <?php echo wp_trim_words( esc_textarea($testimonial->testimonial), 45, '...' ); ?>
                            </p>

                            <p class="attribution">
                                <?php noSeparatorIfLast( array('author', 'company', 'location'), '//', $testimonial->ID ); ?>
                            </p>

                        </div>
                    </div>
                </div>

            </div>

        <?php endforeach; ?>
    </div>

    <?php wp_reset_postdata(); ?>
<?php endif; ?>