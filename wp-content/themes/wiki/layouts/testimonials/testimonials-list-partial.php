<?php if ($testimonials) : ?>

    <?php $index = 1; ?>
    <?php foreach ($testimonials as $testimonial) : ?>

        <?php $title = $testimonial->post_title ? esc_html($testimonial->post_title) : ''; ?>

        <article>

            <div class="entry-date">
                <div class="day"><?php echo date('d', strtotime($testimonial->post_date)); ?></div>
                <div class="month"><?php echo date('M', strtotime($testimonial->post_date)); ?></div>
            </div>

            <h2 class="entry-title">
                <?php echo $title; ?>
            </h2>

            <div class="entry-content">
                <?php echo esc_textarea($testimonial->testimonial); ?>

                <div class="attribution">
                    <?php noSeparatorIfLast( array('author', 'company', 'location'), '//', $testimonial->ID ); ?>
                </div>
            </div>


        </article>
        <?php $index++; ?>
    <?php endforeach; ?>

    <?php wp_reset_postdata(); ?>
<?php endif; ?>