"use strict";

(function( $ ){

    var moduleID = 'bloglist',  // Must equal (data-module)
        module;                 // Holds module element

    var moduleFunctions = {

        init: function()
        {

            var list = module.find('.blog-list');

            if (list.length) {

                var blogUtilForm = module.find('.blog-util form');

                list.ajaxLoader({
                    action: 'blogPosts',
                    form: blogUtilForm
                });

                list.on('loadPosts', function() {
                    lazyImages.refresh();
                });

            }
        },
    };


    // Initialise module
    $.fn['mod' + moduleID] = function () {
        module = $(this);
        moduleFunctions.init();
    };

})( jQuery );