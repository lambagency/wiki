<?php

// Blog Content
// --------------------

$title              = get_the_title();
$postDate           = get_the_date('F j, Y');
$postAuthor         = get_the_author();


// Banner
// --------------------

$bgImageURL = false;
$bgImageMobileURL = false;

$defaultBanner = get_field('background_image', 'option');

if (isset($useFeatureImage) && $useFeatureImage) {
    $bgImageURL = wp_get_attachment_image_src(get_post_thumbnail_id(), 'banner-bg')[0];
    $bgImageMobileURL = wp_get_attachment_image_src(get_post_thumbnail_id(), 'banner-bg-mobile')[0];
}
else {
    $blogPostImage      = get_field('banner_image');
    $bgImageURL         = $blogPostImage ? $blogPostImage['sizes']['banner-bg'] : false;
    $bgImageMobileURL   = $blogPostImage ? $blogPostImage['sizes']['banner-bg-mobile'] : false;
}

$bgImageURL         = $bgImageURL ? : ($defaultBanner ? $defaultBanner['sizes']['banner-bg'] : false);
$bgImageMobileURL   = $bgImageMobileURL ? : ($defaultBanner ? $defaultBanner['sizes']['banner-bg-mobile'] : false);


// Gallery
// --------------------

$gallery            = get_field('gallery');

?>

<div data-module="banner" class="module mod-banner mod-banner-single has-bg size-compact">
    <div class="slider">
        <div class="banner">
            <div class="banner-bg lazy-image" data-lazy-parent data-src="<?php echo $bgImageURL; ?>" data-src-mobile="<?php echo $bgImageMobileURL; ?>"></div>

            <div class="layoutwidth">
                <div class="inner">
                    <h1><?php echo $title; ?></h1>
                    <div class="visible-tablet visible-desktop"><?php the_category(); ?> <span class="post-date"><?php echo $postDate; ?></span></div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include(locate_template( 'layouts/breadcrumbs/breadcrumbs.php' )); ?>


<div class="module mod-blog mod-blog-single">

	<div class="layoutwidth">

        <div class="post-share">
            <?php if (class_exists('EasySocialShareButtons3')) : ?>
                <?php echo do_shortcode('[easy-social-share buttons="facebook,google,twitter,linkedin"]'); ?>
            <?php endif; ?>
        </div>


        <div class="visible-mobile post-details">
            <?php the_category(); ?>
            <span class="post-date"><?php echo $postDate; ?></span>
        </div>


        <div class="blog-content">
            <?php the_content(); ?>
        </div>

        <?php LambAgency\Util\getGallery(array( 'gallery' => $gallery )); ?>
	</div>
</div>