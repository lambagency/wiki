<?php

// Categories
//------------------------------

$category   = array();
$categories = get_the_terms(get_the_ID(), 'category');

if ($categories) {
    $firstCategory = array_pop($categories);

    $category['name']   = $firstCategory->name;
}


// Post Image
//------------------------------

$blogPostImageURL   = false;
$useFeatureImage    = get_field('feature_image');

if (isset($useFeatureImage) && $useFeatureImage) {
    $blogPostImageURL = wp_get_attachment_image_src(get_post_thumbnail_id(), 'square-medium')[0];
}
else {
    $blogPostImage      = get_field('banner_image');
    $blogPostImageURL   = $blogPostImage ? $blogPostImage['sizes']['square-medium'] : false;
}
?>


<div class="blog-post col col-m-1-2 col-l-1-2 col-1-4">

    <div class="inner">
        <div class="post-top">

            <?php if ($blogPostImageURL) : ?>
                <div class="post-image lazy-image" data-src="<?php echo $blogPostImageURL;?>"></div>
            <?php endif; ?>

            <div class="post-detail">
                <?php if ($category) : ?>
                    <span class="category"><?php echo $category['name']; ?></span>
                <?php endif; ?>

                <h4><?php the_title(); ?></h4>
            </div>

            <a href="<?php the_permalink(); ?>" class="link"></a>

        </div>

        <div class="post-bottom">
            <span class="date"><?php echo get_the_date('F d, Y'); ?></span>
        </div>
    </div>
</div>
