"use strict";

(function( $ ){

    var moduleID = 'sidebar', // Must equal (data-module)
        module,                 // Holds module element
        body;
    
    var moduleFunctions = {

        init: function() {
            
            body = $('body');

            moduleFunctions.initSidebarToggle();
            moduleFunctions.initSidebar();
        },
        
        initSidebarToggle: function() {
            var sidebarToggle = $('.sidebar-toggle');

            sidebarToggle.click(function() {
                body.toggleClass('sidebar-open');
            });
        },
        initSidebar: function() {

            var sidebarOverlay = $('.sidebar-overlay');

            sidebarOverlay.click(function() {
                body.removeClass('sidebar-open');
            });
        }
    };


    // Initialise module
    $.fn['component' + moduleID] = function () {
        module = $(this);
        moduleFunctions.init();
    };

})( jQuery );