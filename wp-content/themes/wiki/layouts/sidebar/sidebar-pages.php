<?php

$excludedPages = get_field('side_menu_excluded_pages', 'option');
$excludedPages = implode(',', $excludedPages);

$categoryList = wp_list_pages(array(
    'depth'         => 3,
    'title_li'      => '',
    'echo'          => 0,
    'exclude'       => $excludedPages,
    'sort_column'   => 'post_title'
));

?>
<section class="component sidebar sidebar-pages" data-component="sidebar">
    <ul>
        <?php echo $categoryList; ?>
    </ul>
</section>

<div class="sidebar-overlay"></div>