"use strict";

(function( $ ){

    var moduleID = 'header',    // Must equal (data-module)
        module,                 // Holds module element
        body;


    var moduleFunctions = {

        init: function() {

            var header        = $('header'),
                scrollTopBase = 0,
                scrollTop     = 0,
                minScroll     = 50;

            $(window).scroll(function () {

                var scrollTopNew = $(this).scrollTop();

                if (scrollTopNew > scrollTopBase + minScroll) {
                    header.addClass('compact-a');
                }
                else {
                    header.removeClass('compact-a');
                }

                if (scrollTopNew > scrollTop) {

                    if (scrollTopNew > minScroll) {
                        header.addClass('compact');
                    }
                }
                else {
                    header.removeClass('compact');
                }

                scrollTop = scrollTopNew;
            });
        }
    };


    // Initialise module
    $.fn['mod' + moduleID] = function () {
        module = $(this);
        moduleFunctions.init();
    };

})( jQuery );