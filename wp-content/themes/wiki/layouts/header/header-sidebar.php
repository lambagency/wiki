<?php include(locate_template('layouts/header/header-html.php')); ?>
<body <?php body_class(''); ?>>

<div id="top" class="layout header-fixed">

    <header>
        <div class="layoutwidth">
            <?php if ($headerLogo = get_field('header_logo', 'option')) : ?>
                <div class="header-logo">
                    <a href="<?php echo BASE_URL; ?>" title="<?php bloginfo('name'); ?>">
                        <?php echo LambAgency\Util\svgIcon('lamb'); ?>
                        Wiki
                    </a>
                </div>
            <?php endif; ?>

            <a class="sidebar-toggle"><span></span></a>
        </div>
    </header>

    <div class="layoutwidth sidebar-layout">
        
        <?php get_layout('sidebar', 'pages'); ?>
    
        <div class="main-content has-sidebar">