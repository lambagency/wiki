<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9 ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9]>         <html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?php if (defined('DEV_ENVIRONMENT') && DEV_ENVIRONMENT) : ?>
		<meta name="robots" content="noindex, nofollow"/>
    <?php else : ?>
        <meta name="robots" content="index, follow">
	<?php endif; ?>

	<?php //gravity_form_enqueue_scripts(1, true); ?>

	<?php wp_head(); ?>

</head>

<body <?php body_class(''); ?>>