(function( $ ){
    "use strict";

    var moduleID = 'tabs',  // Must equal (data-module)
        module;             // Holds module element

    var moduleFunctions = {

        init: function() {

            var animationTime   = 200,
                tabs            = module.find('.tab'),
                panels          = module.find('.panel'),
                desktopTabs     = module.find('.tabs-desktop'),
                tabPanels       = module.find('.tab-panels'),
                defaultHeight   = panels.eq(0).outerHeight();


            // if window > tablet portrait, set first tab/panel to 'open'
            if ($.device('min', 'tablet-portrait')) {

                moduleFunctions.setPanelOpen( desktopTabs.find('.tab').eq(0), tabPanels.find('.tab').eq(0), panels.eq(0) );
                tabPanels.css('height', defaultHeight);
            }

            // on window resize, if 'tablet-portrait' and no tabs are opened, default
            // the first tab to open
            $(window).resize(function() {

                if ($.device('min', 'tablet-portrait')) {

                    var openedTabs = module.find('.tab.open').length;

                    if (openedTabs === 0) {
                        moduleFunctions.setPanelOpen( desktopTabs.find('.tab').eq(0), tabPanels.find('.tab').eq(0), panels.eq(0) );
                    }
                }
            });


            // click event for the tabs
            tabs.click(function(e) {
                e.preventDefault();

                var currTab = $(this);

                if (!currTab.hasClass('animating')) {

                    var tabIndex        = currTab.data('tab-item'),
                        currDesktopTab  = desktopTabs.find('.tab').eq(tabIndex),
                        currMobileTab   = tabPanels.find('.tab').eq(tabIndex);

                    // prevent multiple tabs to be opened at same time
                    if (currDesktopTab.hasClass('open') && currMobileTab.hasClass('open')) {

                        // only collapse for less than tablet-landscape
                        if ($.device('max', 'tablet-landscape')) {
                            moduleFunctions.setPanelOpen(currDesktopTab, currMobileTab, panels);
                        }
                    }
                    else {

                        // reset tabs
                        tabs.removeClass('open');
                        panels.removeClass('open');

                        // set open tab/panel
                        currDesktopTab.addClass('open');
                        currMobileTab.addClass('open');


                        var currTabHeight       = panels.eq(tabIndex).outerHeight(),
                            currTabPanels       = currTab.parent().next('.tab-panels');

                        currTabPanels.animate({
                            height: currTabHeight
                        }, 500);


                        setTimeout(function () {
                            currTab.removeClass('animating');

                            // show active content
                            panels.eq(tabIndex).addClass('open');

                        }, animationTime);
                    }
                }

                // trigger resize event to fix tab issue
                window.dispatchEvent(new Event('resize'));
            });
        
        },


        setPanelOpen : function(tab1, tab2, panels) {
            tab1.addClass('open');
            tab2.addClass('open');
            panels.addClass('open');
        }

    };




    // Initialise module
    $.fn['mod' + moduleID] = function () {
        module = $(this);
        moduleFunctions.init();
    };

})( jQuery );