<?php
$fields     = LambAgency\Util\getFields();
$gallery    = get_sub_field('gallery');
?>

<div data-module="gallery" class="module mod-gallery <?php echo implode(' ', $fields['moduleClass']); ?>">

    <?php include(locate_template('layouts/components/background-image/background-image.php')); ?>

    <div class="layoutwidth">

        <?php \LambAgency\Util\getTitleBlock($fields['title-block']); ?>

        <?php include(locate_template('layouts/components/content/content.php')); ?>
        
        <?php LambAgency\Util\getGallery(array( 'gallery' => $gallery )); ?>

        <?php \LambAgency\Util\getButton(array('buttons' => $fields['buttons'])); ?>

    </div>

</div>