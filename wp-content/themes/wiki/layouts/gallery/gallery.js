(function( $ ){
    'use strict'; //place within function, otherwise multiple global declarations redundant

    var moduleID = 'gallery', // Must equal (data-module)
        module;                 // Holds module element

    var moduleFunctions = {

        init: function() {

        }
    };


    // Initialise module
    $.fn['mod' + moduleID] = function () {
        module = $(this);
        moduleFunctions.init();
    };

})( jQuery );