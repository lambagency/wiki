<?php

$fields                 = LambAgency\Util\getFields();
$wayfinderType          = $fields['type'];
$fields['wayfinder']    = \LambAgency\Util\getWayfinderItems($wayfinderType);

include(locate_template('layouts/wayfinder/wayfinder-partial.php'));

?>