# Wayfinder


## Types

* links
    * Allows user to define what page to link to
    
* child
    * Automatically creates a list of child pages
    


## Style Types

* title-overlay
    * Positions the title on top of the background image
    * On hover, transition effect shows
    
    
* title-overlay-summary-hover
    * Positions the title on top of the background image
    * On hover, card flips to show title and summary
    
    
* detail-below
    * Positions the title below the image

