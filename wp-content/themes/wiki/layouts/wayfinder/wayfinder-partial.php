<div data-module="wayfinder" class="module mod-wayfinder <?php echo implode(' ', $fields['moduleClass']); ?>">

    <?php \LambAgency\Util\getTitleBlock($fields['title-block']); ?>

    <?php if (isset($fields['wayfinder']) && $fields['wayfinder']) : ?>
        <div class="wayfinder row">

            <?php $i = 1; ?>

            <?php foreach ($fields['wayfinder'] as $item) : ?>

                <?php list($imgUrl, $title, $content, $link) = \LambAgency\Util\getWayfinderFields($wayfinderType, $item); ?>

                <div class="col col-m-1-2 col-l-1-2 col-1-2">
                    <div class="wayfinder-item">

                        <div class="inside">
                            <h3><?php echo $title; ?></h3>
                        </div>

                        <a href="<?php echo $link; ?>" class="link"></a>

                    </div>

                </div>

                <?php $i++; ?>

            <?php endforeach; ?>
        </div>
    <?php endif; ?>

</div>