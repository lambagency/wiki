<?php

$additionalFields = array(
    'hideMobile' => 'hide_mobile'
);

$fields =  LambAgency\Util\getFields($additionalFields);


// Module Class
//--------------------------

$fields['content'] ? false : ($fields['moduleClass'][] = 'double-padding');

?>

<div data-module="text" class="module mod-text <?php echo implode(' ', $fields['moduleClass']); ?>">

    <?php include(locate_template('layouts/components/background-image/background-image.php')); ?>

    <div class="layoutwidth">

        <?php \LambAgency\Util\getTitleBlock($fields['title-block']); ?>

        <?php include(locate_template('layouts/components/content/content.php')); ?>

        <?php \LambAgency\Util\getButton(array('buttons' => $fields['buttons'])); ?>

    </div>

</div>