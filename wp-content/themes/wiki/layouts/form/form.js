"use strict";

(function( $ ){

    var moduleID = 'form',  // Must equal (data-module)
        module;             // Holds module element

    var moduleFunctions = {

        init: function() {

            this.initSelects();
            this.initFormValidation();
        },


        /**
         * Adds asterisk to required select fields
         * Adds select change event to toggle 'has-value' class
         */
        initSelects: function()
        {
            module.find('select').each(function() {

                var currSelect = $(this);
                var li = currSelect.closest('li');

                if (li.hasClass('gfield_contains_required')) {

                    var firstOption = currSelect.find('option:first-child');
                    var optionText = firstOption.text();
                    firstOption.html(optionText + '<span>*</span>');
                }

                currSelect.on('change.custom-select', function() {

                    currSelect.parent().toggleClass('has-value', currSelect.val() != '');
                });


                // Trigger select change
                currSelect.change();
            });
        },


        /**
         * Switch statement to initialise JS validation for each form
         */
        initFormValidation: function()
        {

            var form = module.find('form'),
                fields = form.find('.gfield');

            form.validate({
                errorPlacement: function(error, element) {
                    error.insertAfter($(element).closest('.ginput_container'));
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass(errorClass).removeClass(validClass);
                    $(element).closest('.ginput_container').addClass(errorClass).removeClass(validClass);
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass(errorClass).addClass(validClass);
                    $(element).closest('.ginput_container').removeClass(errorClass).addClass(validClass);
                }
            });


            $.each(fields, function() {

                var rules = {};
                var inputs = $(this).find('input, select, textarea'),
                    input = inputs.first();


                // Required rules
                //---------------------------

                if ($(this).hasClass('gfield_contains_required')) {
                    var rule = true;

                    if (inputs.length > 1) {

                        // Add custom rule to set checkboxes to min 1 checked
                        if (inputs.attr('type') == 'checkbox') {
                            rule = function() {
                                var checked = false;
                                inputs.each(function() {
                                    if ($(this).is(':checked')) {
                                        checked = true;
                                    }
                                });

                                return !checked;
                            };
                        }
                    }
                    rules['required'] = rule;

                    // Bind multiple inputs to re-validate first input on change
                    inputs.on('change', function() {
                        input.valid();
                    });
                }


                // Additional validation rules
                //---------------------------

                var classList = this.className.split(/\s+/);

                // Loop through all classes, check for additional validation requirements
                for (var i = 0, len = classList.length; i < len; i++) {

                    // VALIDATE MAX WORDS
                    if (/^validate-max-words-/.test(classList[i])) {

                        var maxWords = classList[i].replace('validate-max-words-', '');

                        if (maxWords) {
                            rules['maxWordCount'] = [maxWords];
                        }
                    }

                    // VALIDATE MIN WORDS
                    if (/^validate-min-words-/.test(classList[i])) {

                        var minWords = classList[i].replace('validate-min-words-', '');

                        if (minWords) {
                            rules['minWordCount'] = [minWords];
                        }
                    }
                }

                // Apply rules
                if (!$.isEmptyObject(rules)) {
                    input.rules("add", rules);
                }
            });

        }
    };


    // Initialise module
    $.fn['mod' + moduleID] = function () {
        module = $(this);
        moduleFunctions.init();
    };

})( jQuery );