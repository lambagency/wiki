<?php
$themeSettings  = get_sub_field('theme_settings_values');

if ($themeSettings) {
    $name       = get_bloginfo('name');
    $map        = get_field('map', 'option');
    $street     = get_field('street', 'option');
    $suburb     = get_field('suburb', 'option');
    $state      = get_field('state', 'option');
    $postcode   = get_field('postcode', 'option');
    $phone      = get_field('phone', 'option');
    $email      = get_field('email', 'option');
}
else {
    $name       = get_sub_field('name');
    $map        = get_sub_field('map');
    $street     = get_sub_field('street');
    $suburb     = get_sub_field('suburb');
    $state      = get_sub_field('state');
    $postcode   = get_sub_field('postcode');
    $phone      = get_sub_field('phone');
    $email      = get_sub_field('email');
}



if ($map) : ?>

    <?php wp_enqueue_script('gmap', 'https://maps.googleapis.com/maps/api/js?v=3.exp', [], false, true); ?>

    <div data-module="map" class="module mod-map">

        <div class="acf-map">
            <div class="marker" data-lat="<?php echo $map['lat']; ?>" data-lng="<?php echo $map['lng']; ?>">

                <div class="address">
                    <p>
                        <?php echo $street ? $street . '<br />' : '' ?>
                        <?php echo $suburb ? $suburb : '' ?>
                        <?php echo $state ? $state : '' ?>
                        <?php echo $postcode ? $postcode : '' ?>
                    </p>

                    <p>
                        <?php if ($phone) : ?><a href="tel:<?php echo preg_replace('/[^0-9]/', '', $phone); ?>"><?php echo $phone; ?></a><br /><?php endif; ?>
                        <?php if ($email) : ?><a href="mailto:<?php echo antispambot($email); ?>"><?php echo antispambot($email); ?></a><?php endif; ?>
                    </p>
                </div>


                <script type="application/ld+json">
                {
                    "@context": "http://schema.org",
                    "@type": "Organization",
                    <?php if ($street) : ?>"name": "<?php echo $name; ?>",<?php endif; ?>
                    "address": {
                        "@type": "PostalAddress",
                        <?php if ($street) : ?>"streetAddress": "<?php echo $street; ?>",<?php endif; ?>
                        <?php if ($suburb) : ?>"addressLocality": "<?php echo $suburb; ?>",<?php endif; ?>
                        <?php if ($state) : ?>"addressRegion": "<?php echo $state; ?>",<?php endif; ?>
                        <?php if ($postcode) : ?>"postalCode": "<?php echo $postcode; ?>"<?php endif; ?>
                    },
                    <?php if ($email) : ?>"email": "<?php echo antispambot($email); ?>",<?php endif; ?>
                    <?php if ($phone) : ?>"telephone": "<?php echo $phone; ?>"<?php endif; ?>
                }
                </script>


            </div>
        </div>

    </div>
<?php endif;