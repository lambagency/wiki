<?php

setup_postdata($post);

/**
 * Get the post content, allows returning of formatted post content
 *
 * @param bool $display - if content should be printed or returned
 *
 * @return string Formatted post content
 */
function ob_wp_content($display = true) {
    global $post;
    ob_start();
    the_content();
    $output = ob_get_contents();
    ob_end_clean();
    if(!$display) {return $output;} else {print $output;};
}


// Holds generated index
$articleIndex = array();

// Holds generated index slugs, used to prevent slug duplicates
$articleIndexSlugs = array();


$content = ob_wp_content(false);



// Regular expression for matching heading tags
$re = "/<h(\\d)>(.*)<\\/h\\d>/";

preg_match_all($re, $content, $matches);

foreach ($matches[0] as $index => $match) {

    // Lower case everything, clean multiple spaces
    $headingID = strtolower(preg_replace("/[\s-]+/", " ", $matches[2][$index]));
    // Convert whitespaces and underscore to dash
    $headingID = preg_replace("/[\s_]/", "-", $headingID);
    // Append with prefix to avoid conflicts
    $headingID = 'heading-' . $headingID;

    // Check if heading slug already exists
    if (key_exists($headingID, $articleIndexSlugs)) {

        // Increase count by 1
        $articleIndexSlugs[$headingID] = $articleIndexSlugs[$headingID] + 1;

        $headingID .= '-' . $articleIndexSlugs[$headingID];
    }
    else {
        $articleIndexSlugs[$headingID] = 1;
    }

    $newHeading = vsprintf("<h%d><a class='anchor' id=\"%s\"></a>%s</h%d>", array(
        $matches[1][$index],
        $headingID,
        $matches[2][$index],
        $matches[1][$index]
    ));

    $anchor = vsprintf("<a href=\"#%s\">%s</a>", array(
        $headingID,
        $matches[2][$index]
    ));

    $pos = strpos($content, $match);
    if ($pos !== false) {
        $content = substr_replace($content, $newHeading, $pos, strlen($match));
    }

    // Add to index array if h2 or h3
    if ($matches[1][$index] == 2) {
        $articleIndex[] = array(
            'heading' => $anchor
        );
    }
    else if ($matches[1][$index] == 3) {

        $articleIndexCount = count($articleIndex);

        if ($articleIndexCount > 0) {
            $articleIndex[$articleIndexCount - 1]['subheading'][]['heading'] = $anchor;
        }
    }
}

?>
<div class="module mod-post">

    <article>

        <h1><?php the_title(); ?></h1>

        <div class="article-index">
            <ul>
                <?php foreach ($articleIndex as $indexHeading) : ?>
                    <li>
                        <?php echo $indexHeading['heading']; ?>

                        <?php if (isset($indexHeading['subheading']) && $indexHeading['subheading']) : ?>
                            <ul>
                                <?php foreach ($indexHeading['subheading'] as $indexSubHeading) : ?>
                                    <li><?php echo $indexSubHeading['heading']; ?></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        
        <div class="content">
            <?php echo $content; ?>
        </div>
    </article>
    
</div>