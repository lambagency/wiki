<?php if ( $icons ) : ?>
<div class="icons row" data-slick='{"slidesToShow": 1, "slidesToScroll": 1}'>

    <?php foreach( $icons as $key => $icon ) : ?>

        <?php
        $theIcon      = $icon['icon'];
        $iconURL      = $theIcon['sizes']['square-medium'];
        $iconTitle    = $icon['title'];
        $iconSubtitle = $icon['subtitle'];
        $iconContent  = $icon['content'];

        $animationDelay = 200 + ($key * 200);
        ?>

        <div class="icon-block col col-s-1-1 col-m-1-2 col-l-1-2 col-1-2 animate" <?php echo \LambAgency\Util\addAnimation('fadeInDown', $animationDelay); ?>>
            <div class="icon-image col col-1-4">
                <div class="icon-container">
                    <img src="<?php echo esc_url($iconURL);?>" alt="<?php echo esc_html($iconTitle); ?>">
                </div>
            </div>

            <div class="icon-details col col-3-4">
                <?php if ( !empty($iconTitle) ) : ?>
                    <h4 class="icon-title"><?php echo esc_html($iconTitle); ?></h4>
                <?php endif; ?>

                <?php if ( !empty($iconSubtitle) ) : ?>
                    <p class="icon-subtitle"><?php echo esc_html($iconSubtitle); ?></p>
                <?php endif; ?>

                <?php if ( !empty($iconContent) ) : ?>
                    <p class="icon-content"><?php echo esc_html($iconContent); ?></p>
                <?php endif; ?>
            </div>

            <?php \LambAgency\Util\getButton(array('buttons' => $fields['buttons'])); ?>
        </div>

    <?php endforeach; ?>

</div>
<?php endif; ?>