(function( $ ){
    'use strict'; //place within function, otherwise multiple global declarations redundant

    var moduleID = 'icons', // Must equal (data-module)
        module;                 // Holds module element

    var moduleFunctions = {

        init: function() {
            var dataSlick = module.find('.icons').data('slick'),
                icons = module.find('.icons');

            /**
             * Slick slider if needed
             */
            // if ( !!icons && (typeof icons !== 'undefined') ) {
                // icons.slick({
                //     slidesToShow  : 1,
                //     slidesToScroll: 1,
                //     dots          : true,
                //     autoplay      : true,
                //     autoplaySpeed : 5000,
                //     arrows        : true,
                //     infinite      : false,
                //     responsive    : [
                //         {
                //             breakpoint: 1024,
                //             settings  : dataSlick ||  {
                //                 slidesToShow  : 1,
                //                 slidesToScroll: 1
                //             }
                //         }
                //     ]
                // });
            // }
        }
    };


    // Initialise module
    $.fn['mod' + moduleID] = function () {
        module = $(this);
        moduleFunctions.init();
    };

})( jQuery );