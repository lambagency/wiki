<?php
$fields =  LambAgency\Util\getFields();
$icons = get_sub_field('icons');
?>

<div data-module="icons" class="module mod-icons <?php echo implode(' ', $fields['moduleClass']); ?>">

    <?php include(locate_template( 'layouts/components/background-image/background-image.php' )); ?>

    <div class="layoutwidth">

        <?php \LambAgency\Util\getTitleBlock($fields['title-block']); ?>

        <?php include(locate_template('layouts/icons/icons-' . $fields['type'] . '-partial.php')); ?>

    </div>

</div>