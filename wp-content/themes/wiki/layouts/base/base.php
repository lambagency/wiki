<?php

$fields =  LambAgency\Util\getFields();

?>

<div data-module="base" class="module mod-base <?php echo implode(' ', $fields['moduleClass']); ?>">

    <?php include(locate_template('layouts/components/background-image/background-image.php')); ?>

    <div class="layoutwidth">

        <?php \LambAgency\Util\getTitleBlock($fields['title-block']); ?>

        <?php include(locate_template('layouts/components/content/content.php')); ?>

        <?php \LambAgency\Util\getButton(array('buttons' => $fields['buttons'])); ?>

    </div>

</div>