<div class="menu-top-wrap">

    <div class="menu-container">
        <?php
        if (has_nav_menu('header-top')) :
            wp_nav_menu(array(
                'walker'            => new Custom_Walker_Nav_Menu,
                'theme_location'    => 'header-top',
                'menu_class'        => 'menu menu-top',
                'container'         => ''
            ));
        endif;
        ?>
    </div>

</div>