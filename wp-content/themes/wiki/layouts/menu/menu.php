<?php
/*
 * Add any of the following classes to specify the type of menu:
 *
 */
?>

<div class="menu-wrap menu">

	<a class="mobile-menu-toggle"><span></span></a>

	<nav role="navigation">
        <div class="menu-container">
            <?php
            if (has_nav_menu('header-main')) :
                wp_nav_menu(array(
                    'walker'            => new Custom_Walker_Nav_Menu,
                    'theme_location'    => 'header-main',
                    'menu_class'        => 'menu menu-main',
                    'container'         => ''
                ));
            endif;
            ?>
        </div>
	</nav>

</div>