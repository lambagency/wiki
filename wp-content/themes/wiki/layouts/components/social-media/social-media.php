<?php

if ( isset($templateData) ) :

    if ($templateData['social-platforms']) : ?>

        <?php
        $class = $templateData['circles'] ? ' circles' : '';
        ?>

        <div class="social-media<?php echo $class; ?>">

            <?php foreach ($templateData['social-platforms'] as $platform => $url) : ?>

                <a href="<?php echo $url; ?>" target="_blank" class="social-<?php echo $platform; ?>">
                    <?php echo LambAgency\Util\svgIcon($platform); ?>
                </a>
            <?php endforeach; ?>

        </div>
    <?php endif;

endif;