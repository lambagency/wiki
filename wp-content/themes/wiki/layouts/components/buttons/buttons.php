<?php

if (isset($templateData)) {

    echo $templateData['container'] ? '<div class="actions">' : '';

    foreach ($templateData['buttons'] as $index => $button) {
        lamb_acf_button::renderButton($button);
    }

    echo $templateData['container'] ? '</div>' : '';
}