(function( $ ){
    'use strict';

    var componentID = 'gallery', // Must equal (data-module)
        component;                 // Holds component element

    var componentFunctions = {

        init: function() {
        }
    };


    // Initialise module
    $.fn['component' + componentID] = function () {
        component = $(this);
        componentFunctions.init();
    };

})( jQuery );