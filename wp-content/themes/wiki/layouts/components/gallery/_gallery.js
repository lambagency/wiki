(function( $ ){
    'use strict';

    var componentID = 'gallery', // Must equal (data-module)
        component;                 // Holds module element

    var componentFunctions = {

        init: function() {
            componentFunctions.initBlogGallery();
        },

        initBlogGallery: function(){

            $('a.gallery').featherlightGallery({
                openSpeed: 300,
                previousIcon: '',
                nextIcon: '',

                afterContent: function(element) {

                    var target = this.$currentTarget,
                        targetThumbnail = target.find('.gallery-thumbnail');

                    var dataCaption = targetThumbnail.data('caption'),
                        dataTitle   = targetThumbnail.data('title');

                    //remove previous instance before append
                    this.$instance.find('.caption-block').remove();

                    $('<div>', {
                        'class' : 'caption-block'
                    })
                        .append($('<span>',{
                            'class' : 'title',
                            'text' : dataTitle
                        }))
                        .append($('<span>',{
                            'class' : 'caption',
                            'text' : dataCaption
                        }))
                        .appendTo( this.$instance.find('.featherlight-content') );
                }
            });
        }
    };


    // Initialise module
    $.fn['component' + componentID] = function () {
        component = $(this);
        componentFunctions.init();
    };

})( jQuery );