<?php

if ( isset($templateData) ) :

    if ($templateData['images']) : ?>

        <div data-component="gallery" class="component component-gallery row">

            <?php foreach ($templateData['images'] as $key => $image) : ?>

            <div class="col col-s-1-1 col-m-1-2 col-l-1-4 col-1-4">

                <?php
                    $title          = $image['title'];
                    $caption        = $image['caption'];
                    $altText        = $image['alt'];
                    $size           = $image['sizes'];
                    $thumbnailURL   = $size['medium'];
                    $largeImageURL  = $size['large'];
                ?>

                <a class="gallery" href="<?php echo esc_url($largeImageURL); ?>">
                    <div class="gallery-thumbnail lazy-image"
                         data-src="<?php echo esc_url($thumbnailURL); ?>"
                         data-title = "<?php echo esc_html($title); ?>"
                         data-caption = "<?php echo esc_html($caption); ?>"
                         data-alt = "<?php echo esc_html($altText); ?>">
                    </div>
                </a>

            </div>

            <?php endforeach; ?>

        </div>

    <?php endif;

endif;