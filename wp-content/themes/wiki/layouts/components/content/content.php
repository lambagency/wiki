<?php if (isset($fields['content']) && $fields['content']) : ?>
    <div class="content">
        <?php echo $fields['content']; ?>
    </div>
<?php endif; ?>