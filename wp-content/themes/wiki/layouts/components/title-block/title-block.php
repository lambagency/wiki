<?php

if (isset($templateData)) :

    if ($templateData['subtitle'] || $templateData['title'] || $templateData['intro'] ) : ?>

        <div class="title-block">
            <?php if ($templateData['subtitle']) : ?>
                <div class="subtitle"><?php echo $templateData['subtitle']; ?></div>
            <?php endif; ?>

            <?php if ($templateData['title']) : ?>
                <div class="title h1"><?php echo $templateData['title']; ?></div>
            <?php endif; ?>

            <?php if ($templateData['intro']) : ?>
                <div class="intro"><?php echo $templateData['intro']; ?></div>
            <?php endif; ?>
        </div>

    <?php endif;

endif;