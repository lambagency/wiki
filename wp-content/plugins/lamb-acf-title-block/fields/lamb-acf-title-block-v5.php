<?php

// exit if accessed directly
if (!defined('ABSPATH')) exit;


// check if class already exists
if (!class_exists('lamb_acf_title_block')) {


    class lamb_acf_title_block extends acf_field {

        function __construct($settings)
        {
            $this->name     = 'title_block';
            $this->label    = 'Title Block';
            $this->category = 'Custom';
            $this->defaults = [
                'show_intro' => 1
            ];
            $this->settings = $settings;

            parent::__construct();
        }


        /*
        *  render_field_settings()
        *
        *  Create extra settings for your field. These are visible when editing a field
        *
        *  @type	action
        *  @since	3.6
        *  @date	23/01/13
        *
        *  @param	$field (array) the $field being edited
        *  @return	n/a
        */

        function render_field_settings($field)
        {
            acf_render_field_setting( $field, array(
                'label'			=> 'Show Intro field',
                'instructions'	=> '',
                'type'			=> 'radio',
                'name'			=> 'show_intro',
                'choices'		=> array(
                    1				=> 'Yes',
                    0				=> 'No'
                ),
                'layout'	=>	'horizontal',
            ));

        }


        /*
        *  render_field()
        *
        *  Create the HTML interface for your field
        *
        *  @param	$field (array) the $field being rendered
        *
        *  @type	action
        *  @since	3.6
        *  @date	23/01/13
        *
        *  @param	$field (array) the $field being edited
        *  @return	n/a
        */

        function render_field($field)
        {
            $field      = array_merge($this->defaults, $field);
            
            $fieldName  = esc_attr($field['name']);

            $subtitle   = isset($field['value']['subtitle']) ? esc_attr($field['value']['subtitle']) : '';
            $title      = isset($field['value']['title']) ? $field['value']['title'] : '';
            $intro      = isset($field['value']['intro']) ? esc_attr($field['value']['intro']) : '';
            ?>
            
            <div class="lamb-acf-title-block">
                <div class="acf-label">
                    <div class="acf-input-prepend">Subtitle</div>
                    <div class="acf-input-wrap">
                        <div class="acf-input">
                            <input type="text" name="<?php echo $fieldName . '[subtitle]'; ?>" value="<?php echo $subtitle; ?>">
                        </div>
                    </div>
                </div>

                <div class="acf-label">
                    <div class="acf-input-prepend">Title</div>
                    <div class="acf-input-wrap">
                        <div class="acf-input">
                            <input type="text" name="<?php echo $fieldName . '[title]'; ?>" value="<?php echo $title; ?>">
                        </div>
                    </div>
                </div>

                <?php if ($field['show_intro'] == 1) : ?>
                    <div class="acf-label">
                        <div class="acf-input-prepend">Intro</div>
                        <div class="acf-input-wrap">
                            <div class="acf-input">
                                <textarea name="<?php echo $fieldName . '[intro]'; ?>" rows="6"><?php echo $intro; ?></textarea>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>

            <?php
        }


        /*
        *  input_admin_enqueue_scripts()
        *
        *  This action is called in the admin_enqueue_scripts action on the edit screen where your field is created.
        *  Use this action to add CSS + JavaScript to assist your render_field() action.
        *
        *  @type	action (admin_enqueue_scripts)
        *  @since	3.6
        *  @date	23/01/13
        *
        *  @param	n/a
        *  @return	n/a
        */

        function input_admin_enqueue_scripts() {

            // vars
            $url = $this->settings['url'];
            $version = $this->settings['version'];


            // register & include CSS
            wp_register_style( 'acf-input-lamb-acf-title-block', "{$url}assets/css/input.css", array('acf-input'), $version );
            wp_enqueue_style('acf-input-lamb-acf-title-block');
        }


        /*
        *  update_value()
        *
        *  This filter is applied to the $value before it is saved in the db
        *
        *  @type	filter
        *  @since	3.6
        *  @date	23/01/13
        *
        *  @param	$value (mixed) the value found in the database
        *  @param	$post_id (mixed) the $post_id from which the value was loaded
        *  @param	$field (array) the field array holding all the field options
        *  @return	$value
        */
        function update_value( $value, $post_id, $field ) {

            if ($value['subtitle'] != '') sanitize_text_field($value['subtitle']);
            if ($value['title'] != '') sanitize_text_field($value['title']);
            if ($value['intro'] != '') sanitize_text_field($value['intro']);
            
            return $value;
        }
    }


    // initialize
    new lamb_acf_title_block($this->settings);
}