(function ($) {

    function initialize_field(field) {

        // update select fields to use the chosen styling
        field.find('select').chosen();

        // conditional logic for showing/hiding fields when button type changed
        field.on('change', '.button-type', function () {
            var currType = $(this),
                currTypeVal = currType.val(),
                fields = field.find('.lamb-acf-button .acf-label:not(.all)');

            fields.each(function () {

                var currField = $(this),
                    dataBtnType = currField.data('type');


                if (dataBtnType.indexOf(',') != -1) {
                    var types = dataBtnType.split(','),
                        typeCount = types.length,
                        match = false,
                        i;

                    for (i = 0; i < typeCount; i++) {
                        if (types[i] == currTypeVal) {
                            match = true;
                        }
                    }

                    match ? currField.removeClass('hidden') : currField.addClass('hidden');
                }
                else {
                    dataBtnType == currTypeVal ? currField.removeClass('hidden') : currField.addClass('hidden');
                }

            });

        });
    }


    acf.add_action('ready append', function ($el) {

        // search $el for fields of type 'button'
        acf.get_fields({type: 'button'}, $el).each(function () {
            initialize_field($(this));
        });
    });


    // extend ACF File functionality to work for our new ACF Button
    acf.fields.button = acf.fields.file.extend({
        type: 'button'
    });

})(jQuery);