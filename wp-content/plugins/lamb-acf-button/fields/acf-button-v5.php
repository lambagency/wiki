<?php

class lamb_acf_button extends acf_field
{
    function __construct()
    {
        $this->name     = 'button';
        $this->label    = 'Button';
        $this->category = 'Custom';
        $this->defaults = [
            'post_type'		=> ['page'],
            'taxonomy'		=> [],
            'return_format'	=> 'array',
            'library' 		=> 'all',
            'min_size'		=> 0,
            'max_size'		=> 0,
            'mime_types'	=> ''
        ];
        
        $this->settings = [
            'path'    => WP_PLUGIN_URL . '/lamb-acf-button/',
            'dir'     => WP_PLUGIN_DIR . '/lamb-acf-button/',
            'version' => '1.0.0'
        ];

        add_filter('get_media_item_args',			array($this, 'get_media_item_args'));
        add_filter('wp_prepare_attachment_for_js',	array($this, 'wp_prepare_attachment_for_js'), 10, 3);

        parent::__construct();
    }



    /*
    *  render_field_settings()
    *
    *  Create extra settings for your field. These are visible when editing a field
    *
    *  @type	action
    *  @since	3.6
    *  @date	23/01/13
    *
    *  @param	$field (array) the $field being edited
    *  @return	n/a
    */
    function render_field_settings($field)
    {
        acf_render_field_setting($field, array(
            'label'			=> __('Filter by Post Type', 'acf'),
            'instructions'	=> '',
            'type'			=> 'select',
            'name'			=> 'post_type',
            'choices'		=> acf_get_pretty_post_types(),
            'multiple'		=> 1,
            'ui'			=> 1,
            'allow_null'	=> 1,
            'placeholder'	=> __('All post types', 'acf'),
        ));

        acf_render_field_setting($field, array(
            'label'			=> __('Filter by Taxonomy', 'acf'),
            'instructions'	=> '',
            'type'			=> 'select',
            'name'			=> 'taxonomy',
            'choices'		=> acf_get_taxonomy_terms(),
            'multiple'		=> 1,
            'ui'			=> 1,
            'allow_null'	=> 1,
            'placeholder'	=> __('All taxonomies', 'acf'),
        ));
    }


    /*
	*  render_field()
	*
	*  Create the HTML interface for your field
	*
	*  @param	$field (array) the $field being rendered
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field (array) the $field being edited
	*  @return	n/a
	*/
    function render_field($field)
    {
        $field              = array_merge($this->defaults, $field);

        $fieldName          = esc_attr($field['name']);

        $buttonType         = isset($field['value']['button-type']) ? esc_attr($field['value']['button-type']) : '';
        $buttonLabel        = isset($field['value']['text']) ? $field['value']['text'] : '';
        $link               = isset($field['value']['link']) ? esc_attr($field['value']['link']) : '';
        $external           = isset($field['value']['external']) ? esc_attr($field['value']['external']) : '';
        $anchor             = isset($field['value']['anchor']) ? esc_attr($field['value']['anchor']) : '';
        $email              = isset($field['value']['email']) ? $field['value']['email'] : '';
        $phone              = isset($field['value']['phone']) ? esc_attr($field['value']['phone']) : '';
        $download           = isset($field['value']['download']) ? esc_attr($field['value']['download']) : '';
        $buttonColour       = isset($field['value']['button-colour']) ? esc_attr($field['value']['button-colour']) : '';
        $buttonIcon         = isset($field['value']['button-icon']) ? esc_attr($field['value']['button-icon']) : '';
        $buttonIconPosition = isset($field['value']['button-icon-position']) ? esc_attr($field['value']['button-icon-position']) : '';


        $uploader = acf_get_setting('uploader');

        if ($uploader == 'wp') {
            acf_enqueue_uploader();
        }


        // vars
        $o = array(
            'icon'		=> '',
            'title'		=> '',
            'url'		=> '',
            'filesize'	=> '',
            'filename'	=> '',
        );

        $div = array(
            'class'				=> 'acf-file-uploader acf-cf',
            'data-library' 		=> $field['library'],
            'data-mime_types'	=> $field['mime_types'],
            'data-uploader'		=> $uploader
        );

        if ($download) {

            $file = get_post($download);

            if ($file) {
                $o['icon'] = wp_mime_type_icon($file->ID);
                $o['title']	= $file->post_title;
                $o['filesize'] = @size_format(filesize(get_attached_file($file->ID)));
                $o['url'] = wp_get_attachment_url($file->ID);

                $explode = explode('/', $o['url']);
                $o['filename'] = end($explode);
            }


            // url exists
            if ($o['url']) {
                $div['class'] .= ' has-value';
            }
        }
        
        ?>

        <div class="lamb-acf-button">
            <div class="acf-label all">
                <div class="acf-input-prepend">Button Type</div>
                <div class="acf-input-wrap">
                    <div class="acf-input">
                        <select name="<?php echo $fieldName . '[button-type]'; ?>" class="button-type">
                            <option value="">-- Select --</option>
                            <?php foreach ($this->getButtonTypes() as $val => $label) :
                                $selected = $val == $buttonType ? ' selected="selected"' : '';
                                echo '<option value="' . $val . '"' . $selected. '>' . $label . '</option>';
                            endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>


            <div class="acf-label all">
                <div class="acf-input-prepend">Button Label</div>
                <div class="acf-input-wrap">
                    <div class="acf-input">
                        <input type="text" name="<?php echo $fieldName . '[text]'; ?>" value="<?php echo $buttonLabel; ?>">
                    </div>
                </div>
            </div>


            <div class="acf-label<?php echo $buttonType == 'link' ? '' : ' hidden'; ?>" data-type="link">
                <div class="acf-input-prepend">Page Link</div>
                <div class="acf-input-wrap">
                    <div class="acf-input">
                        <select name="<?php echo $fieldName . '[link]'; ?>">
                            <option value="">-- Select --</option>
                            <?php
                            $groupedLinks = $this->getLinks($field);

                            foreach ($groupedLinks as $type => $groupedLink) :
                                echo '<optgroup label="' . $type . '">';
                                foreach ($groupedLink as $page) :
                                    $selected = $page->ID == $link ? ' selected="selected"' : '';
                                    echo '<option value="' . $page->ID . '"' . $selected . '>' . $page->post_title . '</option>';
                                endforeach;
                                echo '</optgroup>';
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
            </div>


            <div class="acf-label<?php echo $buttonType == 'external' ? '' : ' hidden'; ?>" data-type="external">
                <div class="acf-input-prepend">External Link</div>
                <div class="acf-input-wrap">
                    <div class="acf-input">
                        <input type="url" name="<?php echo $fieldName . '[external]'; ?>" value="<?php echo $external; ?>">
                    </div>
                </div>
            </div>


            <div class="acf-label<?php echo $buttonType == 'link' || $buttonType == 'external' ? '' : ' hidden'; ?>" data-type="link,external">
                <div class="acf-input-prepend">Anchor</div>
                <div class="acf-input-wrap">
                    <div class="acf-input">
                        <input type="text" name="<?php echo $fieldName . '[anchor]'; ?>" value="<?php echo $anchor; ?>">
                    </div>
                </div>
            </div>


            <div class="acf-label<?php echo $buttonType == 'email' ? '' : ' hidden'; ?>" data-type="email">
                <div class="acf-input-prepend">Email Address</div>
                <div class="acf-input-wrap">
                    <div class="acf-input">
                        <input type="email" name="<?php echo $fieldName . '[email]'; ?>" value="<?php echo $email; ?>">
                    </div>
                </div>
            </div>


            <div class="acf-label<?php echo $buttonType == 'phone' ? '' : ' hidden'; ?>" data-type="phone">
                <div class="acf-input-prepend">Phone Number</div>
                <div class="acf-input-wrap">
                    <div class="acf-input">
                        <input type="text" name="<?php echo $fieldName . '[phone]'; ?>" value="<?php echo $phone; ?>">
                    </div>
                </div>
            </div>


            <div class="acf-label<?php echo $buttonType == 'download' ? '' : ' hidden'; ?>" data-type="download">
                <div class="acf-input-prepend">File Download</div>
                <div class="acf-input-wrap">
                    <div class="acf-input">
                        <div <?php acf_esc_attr_e($div); ?>>
                            <div class="acf-hidden">
                                <?php acf_hidden_input(array('name' => $fieldName . '[download]', 'value' => $download, 'data-name' => 'id')); ?>
                            </div>
                            <div class="show-if-value file-wrap acf-soh">
                                <div class="file-icon">
                                    <img data-name="icon" src="<?php echo $o['icon']; ?>" alt=""/>
                                </div>
                                <div class="file-info">
                                    <p>
                                        <strong data-name="title"><?php echo $o['title']; ?></strong>
                                    </p>
                                    <p>
                                        <strong><?php _e('File name', 'acf'); ?>:</strong>
                                        <a data-name="filename" href="<?php echo $o['url']; ?>" target="_blank"><?php echo $o['filename']; ?></a>
                                    </p>
                                    <p>
                                        <strong><?php _e('File size', 'acf'); ?>:</strong>
                                        <span data-name="filesize"><?php echo $o['filesize']; ?></span>
                                    </p>

                                    <ul class="acf-hl acf-soh-target">
                                        <?php if ($uploader != 'basic') : ?>
                                            <li><a class="acf-icon -pencil dark" data-name="edit" href="#"></a></li>
                                        <?php endif; ?>
                                        <li><a class="acf-icon -cancel dark" data-name="remove" href="#"></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="hide-if-value">
                                <?php if ($uploader == 'basic') : ?>

                                    <?php if ($field['value'] && !is_numeric($field['value'])) : ?>
                                        <div class="acf-error-message"><p><?php echo $field['value']; ?></p></div>
                                    <?php endif; ?>

                                    <input type="file" name="<?php echo $fieldName . '[download]'; ?>" id="<?php echo $field['id']; ?>" />

                                <?php else: ?>

                                    <p style="margin:0;"><?php _e('No file selected','acf'); ?> <a data-name="add" class="acf-button button" href="#"><?php _e('Add File', 'acf'); ?></a></p>

                                <?php endif; ?>

                            </div>
                        </div>
<!--                        <input type="url" name="--><?php //echo $fieldName . '[download]'; ?><!--" value="--><?php //echo $download; ?><!--">-->
                    </div>
                </div>
            </div>


            <?php if ($buttonColours = $this->getButtonColours()) : ?>
                <div class="acf-label all">
                    <div class="acf-input-prepend">Button Colour</div>
                    <div class="acf-input-wrap">
                        <div class="acf-input">
                            <select name="<?php echo $fieldName . '[button-colour]'; ?>">
                                <option value="">-- Select --</option>
                                <?php foreach ($buttonColours as $colour) :
                                    $selected = $colour['value'] == $buttonColour ? ' selected="selected"' : '';
                                    echo '<option value="' . $colour['value'] . '"' . $selected . '>' . $colour['label'] . '</option>';
                                endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
            <?php endif; ?>


            <?php if ($buttonIcons = $this->getButtonIcons()) : ?>
                <div class="acf-label all">
                    <div class="acf-input-prepend">Button Icon</div>
                    <div class="acf-input-wrap">
                        <div class="acf-input">
                            <select name="<?php echo $fieldName . '[button-icon]'; ?>">
                                <option value="">-- Select --</option>
                                <?php foreach ($buttonIcons as $icon) :
                                    $selected = $icon['value'] == $buttonIcon ? ' selected="selected"' : '';
                                    echo '<option value="' . $icon['value'] . '"' . $selected . '>' . $icon['label'] . '</option>';
                                endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
            <?php endif; ?>


            <?php if ($buttonIconPositions = $this->getButtonIconPositions()) : ?>
                <div class="acf-label all">
                    <div class="acf-input-prepend">Button Icon Position</div>
                    <div class="acf-input-wrap">
                        <div class="acf-input">
                            <select name="<?php echo $fieldName . '[button-icon-position]'; ?>">
                                <option value="">-- Select --</option>
                                <?php foreach ($buttonIconPositions as $position) :
                                    $selected = $position['value'] == $buttonIconPosition ? ' selected="selected"' : '';
                                    echo '<option value="' . $position['value'] . '"' . $selected . '>' . $position['label'] . '</option>';
                                endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

        </div>
        
        <?php
    }


    /*
	*  validate_value()
	*
	*  This filter is used to perform validation on the value prior to saving.
	*  All values are validated regardless of the field's required setting. This allows you to validate and return
	*  messages to the user if the value is not correct
	*
	*  @type	filter
	*  @date	11/02/2014
	*  @since	5.0.0
	*
	*  @param	$valid (boolean) validation status based on the value and the field's required setting
	*  @param	$value (mixed) the $_POST value
	*  @param	$field (array) the field array holding all the field options
	*  @param	$input (string) the corresponding input name for $_POST value
	*  @return	$valid
	*/
    function validate_value($valid, $value, $field, $input)
    {
        $buttonType = $value['button-type'];

        if ($buttonType == '') {
            return 'Button type is required';
        }

        if ($buttonType == 'link' && $value['link'] == '') {
            return 'Please specify a \'Page Link\'';
        }

        if ($buttonType == 'external' && filter_var($value['external'], FILTER_VALIDATE_URL) === false ) {
            return 'Please specify a valid \'External Link\' (include http/s://)';
        }

        if ($buttonType == 'email' && !is_email($value['email'])) {
            return 'Please specify a valid \'Email Address\'';
        }

        if ($buttonType == 'phone' && $value['phone'] == '') {
            return 'Please specify a \'Phone Number\'';
        }

        if ($buttonType == 'download' && $value['download'] == '') {
            return 'Please specify a \'File Download\'';
        }

        return $valid;
    }


    /*
	*  update_value()
	*
	*  This filter is applied to the $value before it is saved in the db
	*
	*  @type	filter
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$value (mixed) the value found in the database
	*  @param	$post_id (mixed) the $post_id from which the value was loaded
	*  @param	$field (array) the field array holding all the field options
	*  @return	$value
	*/
    function update_value($value, $post_id, $field)
    {
        if (isset($value['text']) && $value['text'] != '') sanitize_text_field($value['text']);
        if (isset($value['external']) && $value['external'] != '') sanitize_text_field($value['external']);
        if (isset($value['anchor']) && $value['anchor'] != '') sanitize_text_field($value['anchor']);
        if (isset($value['email']) && $value['email'] != '') sanitize_email($value['email']);
        if (isset($value['phone']) && $value['phone'] != '') sanitize_text_field($value['phone']);
        if (isset($value['download']) && $value['download'] != '') sanitize_text_field($value['download']);

        return $value;
    }


    /*
	*  format_value()
	*
	*  This filter is appied to the $value after it is loaded from the db and before it is returned to the template
	*
	*  @type	filter
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$value (mixed) the value which was loaded from the database
	*  @param	$post_id (mixed) the $post_id from which the value was loaded
	*  @param	$field (array) the field array holding all the field options
	*
	*  @return	$value (mixed) the modified value
	*/

//    function format_value($value, $post_id, $field)
//    {
//        if (empty($value)) return false;
//
//        if (!is_numeric($value)) return false;
//
//        $value = intval($value);
//
//
//        if ($field['return_format'] == 'url') {
//            return wp_get_attachment_url($value);
//        }
//        elseif ($field['return_format'] == 'array') {
//            return acf_get_attachment( $value );
//        }
//
//        return $value;
//    }


    /*
	*  get_media_item_args
	*
	*  description
	*
	*  @type	function
	*  @date	27/01/13
	*  @since	3.6.0
	*
	*  @param	$vars (array)
	*  @return	$vars
	*/

    function get_media_item_args($vars)
    {
        $vars['send'] = true;
        return($vars);
    }


    /*
	*  wp_prepare_attachment_for_js
	*
	*  this filter allows ACF to add in extra data to an attachment JS object
	*
	*  @type	function
	*  @date	1/06/13
	*
	*  @param	{int}	$post_id
	*  @return	{int}	$post_id
	*/

    function wp_prepare_attachment_for_js($response, $attachment, $meta)
    {
        // default
        $fs = '0 kb';

        // supress PHP warnings caused by corrupt images
        if ($i = @filesize(get_attached_file($attachment->ID))) {
            $fs = size_format($i);
        }

        // update JSON
        $response['filesize'] = $fs;

        return $response;
    }


    /*
	*  input_admin_enqueue_scripts()
	*
	*  This action is called in the admin_enqueue_scripts action on the edit screen where your field is created.
	*  Use this action to add CSS + JavaScript to assist your render_field() action.
	*
	*  @type	action (admin_enqueue_scripts)
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	n/a
	*  @return	n/a
	*/
    function input_admin_enqueue_scripts()
    {
        // register ACF scripts
        wp_register_script('lamb-acf-button', $this->settings['path'] . 'js/input.js', array('acf-input'), $this->settings['version']);
        wp_register_style('lamb-acf-button', $this->settings['path'] . 'css/input.css', array('acf-input'), $this->settings['version']);

        // Chosen
        wp_register_script('chosen', $this->settings['path'] . 'js/chosen.jquery.min.js', array('acf-input', 'jquery'), $this->settings['version']);
        wp_register_style('chosen', $this->settings['path'] . 'css/chosen.min.css', array('acf-input'), $this->settings['version']);

        // scripts
        wp_enqueue_script(array(
            'lamb-acf-button',
            'chosen',
        ));

        // styles
        wp_enqueue_style(array(
            'lamb-acf-button',
            'chosen',
        ));
    }


    private function getButtonTypes()
    {
        return [
            'link'     => 'Link',
            'external' => 'External Link',
            'download' => 'File Download',
            'phone'    => 'Phone',
            'email'    => 'Email'
        ];
    }


    private function getLinks($field)
    {
        $groupedLinks = [];

        if ($field['post_type'] && !empty($field['post_type'])) {
            foreach ($field['post_type'] as $type) {

                $orderBy = $type == 'page' ? 'menu_order' : 'title';

                $groupedLinks[ucwords($type)] = get_posts([
                    'post_type'     => $type,
                    'numberposts'   => -1,
                    'orderby'       => $orderBy,
                    'order'         => 'ASC'
                ]);
            }
        }

        if ($field['taxonomy'] && !empty($field['taxonomy'])) {
            foreach ($field['taxonomy'] as $taxonomyFilter) {

                $taxonomyFilter = explode(':', $taxonomyFilter);

                $groupedLinks[ucwords($taxonomyFilter[0])] = get_posts([
                    'numberposts'   => -1,
                    'orderby'       => 'title',
                    'order'         => 'ASC',
                    'tax_query' => [
                        [
                            'taxonomy'  => $taxonomyFilter[0],
                            'field'     => 'slug',
                            'terms'     => $taxonomyFilter[1]
                        ]
                    ]
                ]);
            }
        }

        return $groupedLinks;
    }


    private function getButtonColours()
    {
        $buttonColours = [];

        if (function_exists('have_rows')) {

            if (have_rows('background_colours', 'option')) {
                while (have_rows('background_colours', 'option')) {
                    the_row();

                    $value = get_sub_field('value');
                    $label = get_sub_field('label');

                    $buttonColours[] = array(
                        'value' => $value,
                        'label' => $label
                    );
                }
            }
        }

        return $buttonColours;
    }


    private function getButtonIcons()
    {
        $buttonIcons = [];

        if (function_exists('have_rows')) {

            if (have_rows('button_icons', 'option')) {
                while (have_rows('button_icons', 'option')) {
                    the_row();

                    $value = get_sub_field('value');
                    $label = get_sub_field('label');

                    $buttonIcons[] = array(
                        'value' => $value,
                        'label' => $label
                    );
                }
            }
        }

        return $buttonIcons;
    }


    private function getButtonIconPositions()
    {
        $buttonIconPositions = [];

        if (!empty($this->getButtonIcons())) {

            $positions = [
                'left'      => 'Left',
                'far-left'  => 'Far Left',
                'right'     => 'Right',
                'far-right' => 'Far Right'
            ];

            foreach ($positions as $key => $position) {
                $buttonIconPositions[] = array(
                    'value' => $key,
                    'label' => $position
                );
            }
        }

        return $buttonIconPositions;
    }


    /**
     * @param array $button
     */
    static function renderButton(array $button)
    {
        if (!empty($button)) {

            $link           = lamb_acf_button::getButtonURL($button);
            $animation      = lamb_acf_button::getButtonAnimation($button);
            $buttonText     = lamb_acf_button::getButtonText($button);
            $buttonClass    = lamb_acf_button::getButtonColour($button) . ' animated';

            echo '        
                <a ' . $link . '
                    class="button bg-hover' . $buttonClass . '"
                    role="button"' . $animation['hold'] . ' ' . \LambAgency\Util\addAnimation($animation['effect'], $animation['delay']) . '>
                    ' . $buttonText . '
                </a>            
            ';
        }
    }


    private static function getButtonText(array $button)
    {
        $type           = isset($button['button-type']) ? $button['button-type'] : 'link';
        $text           = isset($button['text']) ? $button['text'] : '';
        $icon           = isset($button['button-icon']) && $button['button-icon'] != '' ? $button['button-icon'] : '';
        $icon           = $icon ? ($type == 'download' ? 'fa-arrow-down' : '') : '';
        $iconPosition   = isset($button['button-icon-position']) && $button['button-icon-position'] != '' ? $button['button-icon-position'] : 'right';
        $iconElement    = $icon ? '<i class="icon-'. $iconPosition .' fa '. $icon .'"></i>' : '';

        return ($iconPosition == 'left' || $iconPosition == 'far-left') ? ($iconElement . $text) : ($text . $iconElement);
    }


    private static function getButtonColour(array $button)
    {
        return isset($button['button-colour']) && $button['button-colour'] != '' ? ' bg-' . $button['button-colour'] : ' bg-primary';
    }


    private static function getButtonURL(array $button)
    {
        $type   = $button['button-type'];
        $anchor = isset($button['anchor']) && $button['anchor'] != '' ? $button['anchor'] : false;
        $newTab = isset($button['new_tab']) ? $button['new_tab'] : false;

        switch ($type) {

            case 'link' :
                $url    = get_the_permalink($button['link']);;
                $url    = $anchor ? $url . '#' . $anchor : $url;
                break;

            case 'external' :
                $url    = $button['external'];
                $url    = $anchor ? $url . '#' . $anchor : $url;
                $newTab = true;
                break;

            case 'phone' :
                $url    = 'tel:' . preg_replace('/[^0-9]/', '', $button['phone']);
                break;

            case 'email' :
                $url    = 'mailto:' . antispambot($button['email']);
                $newTab = true;
                break;

            case 'download' :
                $url    = wp_get_attachment_url($button['download']);;
                $newTab = true;
                break;

            default :
                $url    = '';
        }

        $link  = $url ? 'href="' . $url . '"' : '';
        $link .= $newTab ? ' target="_blank"' : '';

        return $link;
    }


    private static function getButtonAnimation(array $button)
    {
        $animation  = [
            'effect'    => $button['animation'],
            'delay'     => $button['delay'],
            'hold'      => $button['hold'] ? ' data-animate-hold' : ''
        ];

        return $animation;
    }
}

// create field
new lamb_acf_button();