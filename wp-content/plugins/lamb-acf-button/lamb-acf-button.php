<?php

/**
Plugin Name: Advanced Custom Fields: Button
Description: Adds a custom button type
Version: 1.0.0
Author: Lamb Agency
Author URI: http://www.lambagency.com.au
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
**/


function include_field_types_button($version) {
    include_once('fields/acf-button-v5.php');
}

add_action('acf/include_field_types', 'include_field_types_button');