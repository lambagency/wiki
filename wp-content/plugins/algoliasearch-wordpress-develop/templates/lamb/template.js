algoliaBundle.$(document).ready(function ($) {

    /*****************
     **
     ** INITIALIZATION
     **
     *****************/

    var algolia_client = algoliaBundle.algoliasearch(algoliaConfig.app_id, algoliaConfig.search_key);

    /*****************
     **
     ** HELPERS
     **
     *****************/

    function customTransformHit(hit) {

        var highlightMatchIndex = '';
        var contentMatch = '';

        var fields = ['h2', 'h3', 'h4', 'text'];
        var titleFields = ['h2', 'h3', 'h4'];

        if (hit.post_type != 'page' && hit.post_type != 'post')
            return hit;

        /**
         * Reconstructing the content attribute
         * that has been split at indexing time for better relevance
         */

        var highlight_hit = hit._highlightResult;

        fields: for (var i = 0; i < fields.length; i++) {

            if (highlight_hit.post_content[fields[i]] != undefined) {

                for (var j = 0; j < highlight_hit.post_content[fields[i]].length; j++) {

                    // Match
                    if (highlight_hit.post_content[fields[i]][j].value.matchedWords.length > 0) {

                        if (fields[i] == 'text') {
                            contentMatch = {
                                order: hit.post_content[fields[i]][j].order,
                                value: highlight_hit.post_content[fields[i]][j].value.value
                            }
                        }

                        highlightMatchIndex = hit.post_content[fields[i]][j].order;

                        // Break - ensures first match is shown
                        break fields;
                    }
                }
            }
        }


        /**
         * Create list of titles in order of appearance
         */
        var titleKeys = []; // Holds title array keys in reverse
        var titles = []; // Holds titles

        for (var i = 0; i < titleFields.length; i++) {
            if (highlight_hit.post_content[titleFields[i]] != undefined) {

                for (var j = 0; j < hit.post_content[titleFields[i]].length; j++) {

                    titles[hit.post_content[titleFields[i]][j].order] = {
                        heading: titleFields[i],
                        depth: i,
                        value: highlight_hit.post_content[fields[i]][j].value.value
                    };

                    titleKeys.push(hit.post_content[titleFields[i]][j].order);
                }
            }
        }

        titleKeys.sort(function(a, b){return b-a});


        /**
         * Construct title array based on matches title hierarchy.
         */

        var currentDepth = 99;
        var titleContent = '';
        var titleContentArray = [];

        for (var i = 0; i < titleKeys.length; ++i) {
            if ((highlightMatchIndex) >= titleKeys[i] && (currentDepth > titles[titleKeys[i]].depth)) {
                titleContentArray.unshift(titles[titleKeys[i]]);
                currentDepth = titles[titleKeys[i]].depth;

                // Force break when top level reached (h2)
                if (currentDepth == 0) {
                    break;
                }
            }
        }


        /**
         * Construct title string based constructed title hierarchy.
         */

        for (var i = 0; i < titleContentArray.length; ++i) {
            titleContent += '<span class="heading heading-' + titleContentArray[i].heading + '">' + titleContentArray[i].value + '</span>'
        }


        hit._highlightResult.highlight = {};
        hit._highlightResult.highlight.title = titleContent;
        hit._highlightResult.highlight.content = contentMatch.value;
        
        return hit;
    }


    /*****************
     **
     ** AUTOCOMPLETE
     **
     *****************/

    if (algoliaConfig.autocompleteTypes.length > 0 || algoliaConfig.additionalAttributes.length > 0)
    {
        var $autocompleteTemplate = algoliaBundle.Hogan.compile($('#search_suggestion_template').text());
        var $emptyAutocompleteTemplate = algoliaBundle.Hogan.compile($('#search_empty_template').html());

        var hogan_objs = [];

        var sections = ['autocompleteTypes', 'additionalAttributes'];

        for (var j = 0; j < sections.length; j++)
        {
            for (var i = 0; i < algoliaConfig[sections[j]].length; i++)
            {
                var index = algolia_client.initIndex(algoliaConfig.index_prefix + algoliaConfig[sections[j]][i].name);
                var label = algoliaConfig[sections[j]][i].label ? algoliaConfig[sections[j]][i].label : algoliaConfig[sections[j]][i].name;

                hogan_objs.push({
                    source: index.ttAdapter({hitsPerPage: algoliaConfig[sections[j]][i].nb_results_by_section}),
                    displayKey: 'title',
                    templates: {
                        suggestion: function (hit) {

                            customTransformHit(hit);

                            return $autocompleteTemplate.render(hit);
                        },
                        empty: $emptyAutocompleteTemplate.render.bind($emptyAutocompleteTemplate)
                    }
                });
            }
        }


        $(algoliaConfig.search_input_selector).each(function (i) {

            $(this).autocomplete({hint: false}, hogan_objs);

            $(this).on('autocomplete:selected', function (e, item) {
                autocomplete = false;
                instant = false;
                window.location.href = item.permalink;
            });
        });
    }


    /*****************
     **
     ** INSTANT SEARCH
     **
     *****************/

    if (algoliaConfig.instantTypes.length > 0 && (algoliaConfig.is_search_page === '1' || algoliaConfig.autocompleteTypes.length <= 0))
    {
        var instant_selector = !algoliaConfig.autocompleteTypes.length > 0 ? "#search" : "#instant-search-bar";
        var wrapperTemplate = algoliaBundle.Hogan.compile($('#instant_wrapper_template').html());

        $(algoliaConfig.instant_jquery_selector).html(wrapperTemplate.render({ second_bar: (algoliaConfig.autocompleteTypes.length > 0) })).show();

        /** Initialise instant search **/
        var search = algoliaBundle.instantsearch({
            appId: algoliaConfig.app_id,
            apiKey: algoliaConfig.search_key,
            indexName: algoliaConfig.index_prefix + 'all'
        });
        /** Search bar **/
        search.addWidget(
            algoliaBundle.instantsearch.widgets.searchBox({
                container: instant_selector,
                placeholder: 'Search for products'
            })
        );
        /** Stats **/
        var instantStatsTemplate = $('#instant-stats-template').html();
        search.addWidget(
            algoliaBundle.instantsearch.widgets.stats({
                container: '#algolia-stats',
                template: instantStatsTemplate
            })
        );
        /** Sorts **/
        var sorting_indices = algoliaConfig.sorts;
        $.map(sorting_indices, function(index) {
            index.name = algoliaConfig.index_prefix + index.name + '_' + index.sort;
        });
        sorting_indices.unshift({name: algoliaConfig.index_prefix + 'all', label: 'Relevance'});

        search.addWidget(
            algoliaBundle.instantsearch.widgets.indexSelector({
                container: '#algolia-sorts',
                indices: sorting_indices,
                cssClass: 'form-control'
            })
        );
        /** Hits **/
        var instantHitTemplate = $('#instant-hit-template').html();
        search.addWidget(
            algoliaBundle.instantsearch.widgets.hits({
                container: '#instant-search-results-container',
                templates: {
                    empty: '<div class="no-results">No results found matching "<strong>{{query}}</strong>". <span class="button clear-button">Clear query and filters</span> </div>',
                    hit: instantHitTemplate
                },
                transformData: {
                    hit: transformHit.bind(null, $)
                },
                hitsPerPage: algoliaConfig.hitsPerPage
            })
        );

        /** Facets **/
        var facets = algoliaConfig.facets;
        var wrapper = document.getElementById('instant-search-facets-container');
        var facetTemplate = $('#facet-template').html();

        $.each(facets, function (i, facet) {
            facet.template = facetTemplate;
            facet.wrapper = wrapper;
            search.addWidget(getFacetWidget(facet, {
                item: facet.template,
                header: '<div class="facet"><div class="name">' + (facet.label ? facet.label : facet.name) + '</div></div>',
                footer: '</div/></div>'
            }));
        });

        /** Pagination **/
        search.addWidget(
            algoliaBundle.instantsearch.widgets.pagination({
                container: '#instant-search-pagination-container',
                cssClass: 'algolia-pagination',
                showFirstLast: false,
                labels: {
                    prev: '‹', // &lsaquo;
                    next: '›', // &rsaquo;
                    first: '«', // &laquo;
                    last: '»' // &raquo;
                }
            })
        );
        /** Url sync **/
        search.addWidget(
            algoliaBundle.instantsearch.widgets.urlSync({
                useHash: true,
                threshold: 5000,
                trackedParameters: ['query', 'page', 'attribute:*', 'index']
            })
        );
        search.start();
    }

    $(algoliaConfig.search_input_selector).attr('autocomplete', 'off').attr('autocorrect', 'off').attr('spellcheck', 'false').attr('autocapitalize', 'off');
});
