<?php
/**
 * Plugin Name: Lamb Dashboard
 * Version: 1.0
 * Description: Custom Dashboard
 * Author: Lamb Agency
 * Author URI: http://www.lambagency.com.au
 * Plugin URI: http://www.lambagency.com.au
 * Text Domain: lamb-functions
 * Domain Path: /languages
 * @package Lamb Functions
 */


new LambDashboard();


class LambDashboard
{
    public function __construct()
    {
        add_action('wp_dashboard_setup', array($this, 'addDashboardWidgets'));
    }

    
    public function addDashboardWidgets()
    {
        $siteName = get_bloginfo('name');
        wp_add_dashboard_widget('lambDashboard', $siteName . ' Dashboard', array(&$this, 'renderDashboardWidget'));
    }


    public function renderDashboardWidget()
    {
        $content = get_field('lamb_dashboard_content', 'option');

        if ($content) {
            echo '<div class="dashboard-content">' . $content . '</div>';
        }
    }
}