<?php
/**
 * Plugin Name: Lamb Functions
 * Version: 0.3
 * Description: Theme functions
 * Author: Lamb Agency
 * Author URI: http://www.lambagency.com.au
 * Text Domain: lamb-functions
 * Domain Path: /languages
 * @package Lamb Functions
 */


// http://w-shadow.com/blog/2010/09/02/automatic-updates-for-any-plugin/
require 'plugin-update-checker/plugin-update-checker.php';
$updateChecker = PucFactory::buildUpdateChecker('http://dev.lambagency.com.au/wp-plugins/lamb-functions.json', __FILE__);



require_once('functions/acf.php');
require_once('functions/ajax.php');
require_once('functions/comments.php');
require_once('functions/error-logging.php');
require_once('functions/gravityforms.php');
require_once('functions/error-logging.php');
require_once('functions/framework.php');
require_once('functions/login.php');
require_once('functions/media-taxonomy.php');
require_once('functions/menus.php');
require_once('functions/misc.php');
require_once('functions/posttype.php');
require_once('functions/register.php');
require_once('functions/search.php');
require_once('functions/shortcodes.php');
require_once('functions/soil.php');
require_once('functions/template-tags.php');
require_once('functions/thumbnails.php');
require_once('functions/user.php');
require_once('functions/util.php');
require_once('functions/widgets-init.php');