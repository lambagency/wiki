<?php

namespace LambAgency;

function addLambAgencyRole()
{
    global $wp_roles;

    if (!isset($wp_roles)) {
        $wp_roles = new \WP_Roles();
    }
    
    $lambAgencyRole = 'lamb-agency';
    
    if (!$wp_roles->is_role($lambAgencyRole)) {
        $administrator = $wp_roles->get_role('administrator');

        $wp_roles->add_role($lambAgencyRole, 'Lamb Agency', $administrator->capabilities);
    }
}
add_action('init', __NAMESPACE__ . '\addLambAgencyRole');