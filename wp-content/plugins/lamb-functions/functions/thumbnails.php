<?php

/**
 * Enable support for Post Thumbnails
 */
function initThumbnails()
{    
    add_theme_support('post-thumbnails');
    
    add_image_size('header-logo', 300, 150, false);
    add_image_size('footer-logo', 225, 112, false);

    add_image_size('banner-bg', 1800, 600, false);
    add_image_size('banner-bg-mobile', 1000, 600, false);

    add_image_size('module-bg', 1800, 600, false);
    add_image_size('module-bg-mobile', 1000, 600, false);

    add_image_size('square-small', 200, 200, false);
    add_image_size('square-medium', 600, 600, false);
}
add_action('after_setup_theme', 'initThumbnails');


/** change WP default jpeg compression from 90 to 80 **/
add_filter('jpeg_quality', create_function('', 'return 80;'));