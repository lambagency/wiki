<?php

/**
 * Looks at the layout folder and picks out the data requested
 *
 * @param null  $name
 * @param null  $key
 * @param array $params
 */
function get_layout($name = null, $key = null, $params = array())
{
    do_action('get_layout', $name);

    $templates = array();
    $name = (string)$name;
    if ('' !== $name) {

        if ('' !== $key) {
            $templates[] = "layouts/{$name}/{$name}-{$key}.php";
        }

        $templates[] = "layouts/{$name}/{$name}-default.php";
        $templates[] = "layouts/{$name}/{$name}.php";

        if ('' !== $key) {
            $templates[] = "layouts/{$name}-{$key}.php";
        }

        $templates[] = "layouts/{$name}.php";
    }

    locate_template($templates, true, false);
}


/**
 * Renders out layouts from specified flexible content field
 *
 * @param      $flexibleContent
 * @param bool $tabs
 */
function getFlexibleContent($flexibleContent, $tabs = false)
{
    if (have_rows($flexibleContent)) {

        while (have_rows($flexibleContent)) {

            the_row();

            $layout = get_row_layout();

            // split layout name by underscore
            $layoutComponents = explode('_', $layout, 2);

            // if layout key defined in layout name
            if (count($layoutComponents) > 1) {
                $key = $layoutComponents[1];
            } else {
                // Get type from select box
                $type = get_sub_field('type');
                $key = $type ? strtolower(trim($type)) : 'default';
            }

            $name = $layoutComponents[0];
            $key = $key == 'default' ? '' : $key;

            get_layout($name, $key);
        }

    } else {

        if (!$tabs) {
            get_layout('banner');

            if (defined('DEV_ENVIRONMENT') && DEV_ENVIRONMENT) {
                get_layout('empty');
            }
        }
    }
}