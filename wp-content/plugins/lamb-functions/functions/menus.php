<?php

function registerMenus()
{
    register_nav_menus(array(
        'header-top'    => __('Header - Top', 'header-top'),
        'header-main'   => __('Header - Main', 'header-main'),
        'footer-main'   => __('Footer - Main', 'footer-main'),
        'footer-legals' => __('Footer - Legals', 'footer-legals'),
    ));
}
add_action('after_setup_theme', 'registerMenus');


/**
 * Add Lamb Agency link to footer legals menu
 *
 * @param $items
 * @param $args
 *
 * @return string
 */
function addLambLinkToLegalsMenu($items, $args)
{
    if (in_array($args->theme_location, ['footer-legals'])) {
        $items .= '<li><a href="http://www.lambagency.com.au" target="_blank"' . (!is_front_page() ? ' rel="nofollow"' : '') . '>Lamb Agency</a></li>';
    }

    return $items;
}
add_filter('wp_nav_menu_items', 'addLambLinkToLegalsMenu', 10, 2);


class Custom_Walker_Nav_Menu extends Walker_Nav_Menu
{

    public function start_lvl(&$output, $depth = 0, $args = array())
    {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<span class='sub-menu-toggle'></span><ul class='sub-menu'>\n";
    }


    public function end_lvl(&$output, $depth = 0, $args = array())
    {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul>\n";
    }
}
