<?php


function setup()
{
    // Add RSS feed links to <head> for posts and comments.
    add_theme_support('automatic-feed-links');

}
add_action('after_setup_theme', 'setup');


/**
 * Adds Page Slug Body Class
 *
 * @param $classes
 *
 * @return array
 */
function add_slug_body_class($classes)
{
    global $post;
    if (isset($post)) {
        $classes[] = $post->post_type . '-' . $post->post_name;
    }

    return $classes;
}
add_filter('body_class', 'add_slug_body_class');