<?php

/**
 * Custom error logging function
 */
if (!function_exists('write_log')) {
    function write_log($log)
    {
        if (true === WP_DEBUG) {
            if (is_array($log) || is_object($log)) {
                error_log(print_r($log, true));
            } else {
                error_log($log);
            }
        }
    }
}


/**
 * Simply function to print formatted variable to screen
 *
 * @param $data
 */
if (!function_exists('dump')) {
    function dump($data)
    {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }
}



require ABSPATH . 'vendor/autoload.php';


/**
 * Class APILog
 *
 * Uses Monolog which implements the PSR-3 Logger Interface
 *
 * https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-3-logger-interface.md
 * https://github.com/Seldaek/monolog
 * 
 * @Examples:
 *          
 * $logger = new APILog();
 * $logger->info('Info');
 * $logger->error('Error');
 * $logger->critical('Critical', ['exception'=>['a'=>123]]);
 */
class APILog
{
    private $logger;


    public function __construct()
    {
        $logger = new Monolog\Logger('Default');
        $logger->pushHandler(new Monolog\Handler\StreamHandler(LOG_FILE_NAME));

        if (LOG_DB) {
            $dsn = 'mysql:dbname=' . DB_NAME . ';host=' . DB_HOST;
            $logger->pushHandler(new PDOHandler(new PDO($dsn, DB_USER, DB_PASSWORD)));
        }

        $this->logger = $logger;
    }


    /**
     * Logs with an arbitrary level.
     *
     * @param string $message
     * @param array  $context
     */
    public function info($message, array $context = [])
    {
        $this->logger->info($message, $context);
    }


    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string $message
     * @param array  $context
     */
    public function error($message, array $context = [])
    {
        $this->logger->error($message, $context);
    }


    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string $message
     * @param array  $context
     */
    public function critical($message, array $context = [])
    {
        $this->logger->critical($message, $context);
        $this->sendEmail($this->toString($message, $context));
    }


    /**
     * Sends an email with the logged information
     *
     * @param string $bodyContent
     * @param string $emailAddress
     */
    public function sendEmail($bodyContent, $emailAddress = '')
    {
        if (function_exists('wp_mail')) {

            $siteName   = get_option('blogname');

            $to         = $emailAddress ? $emailAddress : ERROR_EMAIL;
            $fromEmail  = 'dev@lambagency.com.au';
            $headers[]  = 'From: ' . $siteName . ' <' . $fromEmail . '>';
            $headers[]  = 'Content-Type: text/html; charset=UTF-8';
            $subject    = $siteName . ' - Log Notification';
            $body       = '<p>The following is a log notification sent from ' . $siteName . '</p>';
            $body      .= '<pre>' . $bodyContent . '</pre>';

            wp_mail($to, $subject, $body, $headers);
        }
    }


    private function toString($message, array $context = [])
    {
        return $message . ($context ? "\n\n" . 'Context: ' . json_encode($context, JSON_PRETTY_PRINT) : '');
    }
}



class PDOHandler extends Monolog\Handler\AbstractProcessingHandler
{
    private $initialized = false;
    private $pdo;
    private $statement;


    public function __construct(PDO $pdo, $level = Monolog\Logger::DEBUG, $bubble = true)
    {
        $this->pdo = $pdo;
        parent::__construct($level, $bubble);
    }


    protected function write(array $record)
    {
        if (!$this->initialized) {
            $this->initialize();
        }

        $this->statement->execute(array(
            'channel'   => $record['channel'],
            'level'     => $record['level'],
            'message'   => $record['formatted'],
            'time'      => $record['datetime']->format('U'),
        ));
    }


    private function initialize()
    {
        $this->pdo->exec(
            'CREATE TABLE IF NOT EXISTS ' . LOG_DB_NAME . ' (channel VARCHAR(255), level INTEGER, message LONGTEXT, time INTEGER UNSIGNED)'
        );

        $this->statement = $this->pdo->prepare(
            'INSERT INTO ' . LOG_DB_NAME . ' (channel, level, message, time) VALUES (:channel, :level, :message, :time)'
        );

        $this->initialized = true;
    }
}