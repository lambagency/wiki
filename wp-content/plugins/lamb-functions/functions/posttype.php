<?php

function create_post_type() {
    register_post_type('testimonials',
        array(
            'labels' => array(
                'name'                  => __('Testimonials'),
                'singular_name'         => __('Testimonial'),
                'add_new'               => __('Add New'),
                'add_new_item'          => __('Add New Testimonial'),
                'edit_item'             => __('Edit Testimonial'),
                'new_item'              => __('New Testimonial'),
                'all_items'             => __('All Testimonials'),
                'view_item'             => __('View Testimonial'),
                'search_items'          => __('Search Testimonials'),
                'not_found'             => __('No Testimonials found'),
                'not_found_in_trash'    => __('No Testimonials found in Trash'),
                'parent_item_colon'     => '',
                'menu_name'             => 'Testimonials'
            ),

            'publicly_queryable'    => true,
            'public'                => true,
            'show_ui'               => true,
            'hierarchical'          => false,
            'menu_position'         => null,
            'query_var'             => true,
            'rewrite'               => array('slug' => 'testimonials', 'with_front' => false),
            'supports'              => array('title','thumbnail'),
            'taxonomies'            => array('post_tag'),
            'has_archive'           => false
        )
    );

    //	register_post_type('clients',
    //	    array(
    //
    //	        'labels' => array(
    //	            'name'                  => __('Clients'),
    //	            'singular_name'         => __('Client'),
    //	            'add_new'               => __('Add New'),
    //	            'add_new_item'          => __('Add New Client'),
    //	            'edit_item'             => __('Edit Client'),
    //	            'new_item'              => __('New Client'),
    //	            'all_items'             => __('All Clients'),
    //	            'view_item'             => __('View Client'),
    //	            'search_items'          => __('Search Clients'),
    //	            'not_found'             => __('No Clients found'),
    //	            'not_found_in_trash'    => __('No Clients found in Trash'),
    //	            'parent_item_colon'     => '',
    //	            'menu_name'             => 'Clients'
    //	        ),
    //
    //	        'publicly_queryable'    => true,
    //	        'public'                => true,
    //	        'show_ui'               => true,
    //	        'hierarchical'          => false,
    //	        'menu_position'         => null,
    //	        'query_var'             => true,
    //	        'rewrite'               => array('slug' => 'clients', 'with_front' => false),
    //	        'supports'              => array('title','editor','thumbnail'),
    //	        'taxonomies'            => array('post_tag'),
    //	        'has_archive'           => false
    //	    )
    //	);
}
add_action( 'init', 'create_post_type' );
