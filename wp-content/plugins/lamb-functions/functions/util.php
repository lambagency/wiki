<?php

namespace LambAgency\Util
{

    /**
     * Returns the common fields for the module in an array format.
     * Also allows additional fields to be retrieved.
     *
     *
     * @param array $additionalFields          - An associative array of additional fields to be retrieved.
     *                                         This array follows the structure $fieldName => $acfFieldKey
     *                                         (e.g. 'hideMobile => 'hide_mobile')
     *
     * @param array $additionalOptionsFields   - An associative array of additional options fields to be retrieved.
     *                                         This array follows the structure $fieldName => $acfFieldKey
     *                                         (e.g. 'hideMobile => 'hide_mobile')
     *
     * @return array - The array of fields
     */
    function getFields($additionalFields = array(), $additionalOptionsFields = array())
    {

        // Custom fields
        //------------------

        $fields = array();

        $fields['type']             = get_sub_field('type');
        $fields['styleType']        = get_sub_field('style_type');
        $fields['title-block']      = get_sub_field('title_block');
        $fields['content']          = get_sub_field('content');

        $fields['bgColour']         = get_sub_field('background_colour');

        $fields['bgImage']          = get_sub_field('background_image');
        $fields['bgImageURL']       = $fields['bgImage'] ? $fields['bgImage']['sizes']['module-bg'] : '';
        $fields['bgImageMobileURL'] = $fields['bgImage'] ? $fields['bgImage']['sizes']['module-bg-mobile'] : '';

        $fields['buttons']          = get_sub_field('buttons');
        $fields['button']           = get_sub_field('button');
        

        // Loop through additional fields
        foreach ($additionalFields as $key => $additionalField) {

            // Ensure associative array
            if (is_string($key)) {
                $fields[(string)$key] = get_sub_field($additionalField);
            }
        }
        

        // Loop through additional options fields
        foreach ($additionalOptionsFields as $key => $additionalOptionsField) {

            // Ensure associative array
            if (is_string($key)) {
                $fields[(string)$key] = get_field($additionalOptionsField, 'option');
            }
        }


        // set default type
        if (!$fields['type']) {
            $fields['type'] = 'default';
        }


        // Module Class
        //------------------

        $fields['moduleClass'] = array();

        // add type to classes
        $fields['moduleClass'][] = $fields['type'];
        $fields['moduleClass'][] = $fields['styleType'];

        (isset($fields['bgColour']) && $fields['bgColour']) ? $fields['moduleClass'][] = 'bg-' . $fields['bgColour'] : false; // Background Colour
        (isset($fields['bgImage']) && $fields['bgImage']) ? $fields['moduleClass'][] = 'has-bg' : false; // Background Image
        (isset($fields['hideMobile']) && $fields['hideMobile']) ? ($fields['moduleClass'][] = 'hide-mobile') : false; // Hide Mobile


        return $fields;
    }


    /**
     * Outputs animation string
     *
     * @param string $effect
     * @param int    $delay
     * @param int    $duration
     * @param int    $vertOffset
     *
     * @return string
     */
    function addAnimation($effect = 'fadeIn', $delay = 0, $duration = 0, $vertOffset = 0)
    {

        $animateStr = 'data-animate="' . $effect . '"';
        $animateStr .= $delay != 0 ? ' data-animate-delay="' . $delay . '"' : '';
        $animateStr .= $duration != 0 ? ' data-animate-duration="' . $duration . '"' : '';
        $animateStr .= $vertOffset != 0 ? ' data-appear-top-offset=" ' . $vertOffset . '"' : '';

        return $animateStr;
    }

    /**
     * Get link set in theme settings by name
     *
     * @param $linkName - The link identifying name
     *
     * @return string - The link URL
     */
    function getLink($linkName)
    {
        $linkURL = '';

        if (isset($linkName) && $linkName != '') {

            $links = get_field('links', 'option');

            foreach ($links as $link) {

                if ($link['name'] == $linkName) {
                    $linkURL = $link['link'];

                    break;
                }
            }
        }

        return $linkURL;
    }


    /**
     * Generate anchor link for email address
     *
     * @param string $emailAddress       - The email address
     * @param string $innerHTML          - Inner HTML to prepend to the anchor content
     * @param string $innerHTMLPlacement - The placement of the optional inner html (prepend|append|override)
     *
     * @return string   - The email address anchor html
     */
    function emailAnchor($emailAddress = '', $innerHTML = '', $innerHTMLPlacement = 'prepend')
    {
        $emailHTML = '<a href="mailto:' . antispambot($emailAddress) . '" target="_blank" rel="noopener noreferrer" class="email">';

        // Override Inner HTML
        if ($innerHTMLPlacement == 'override') {
            $emailHTML .= $innerHTML;
        }
        // Append Inner HTML
        else {
            if ($innerHTMLPlacement == 'append') {
                $emailHTML .= antispambot($emailAddress) . $innerHTML;
            }
            // Prepend Inner HTML
            else {
                $emailHTML .= $innerHTML . antispambot($emailAddress);
            }
        }

        $emailHTML .= '</a>';

        return $emailHTML;
    }


    /**
     * Echo generated email anchor
     *
     * @param string $emailAddress       - The email address
     * @param string $innerHTML          - Inner HTML to prepend to the anchor content
     * @param string $innerHTMLPlacement - The placement of the optional inner html (prepend|append|override)
     *
     * @return string   - Echo out email address anchor html
     */
    function theEmailAnchor($emailAddress = '', $innerHTML = '', $innerHTMLPlacement = 'prepend')
    {
        $emailHTML = $emailAddress ? emailAnchor($emailAddress, $innerHTML, $innerHTMLPlacement) : '';

        echo $emailHTML;
    }


    /**
     * Generate anchor link for phone number
     *
     * @param string $phoneNumber        - The phone number
     * @param string $innerHTML          - Inner HTML to prepend to the anchor content
     * @param string $innerHTMLPlacement - The placement of the optional inner html (prepend|append|override)
     *
     * @return string   - The phone number anchor html
     */
    function phoneAnchor($phoneNumber = '', $innerHTML = '', $innerHTMLPlacement = 'prepend')
    {
        $phoneLink = preg_replace('/[^0-9]/', '', $phoneNumber); // Remove all characters except digits

        $phoneHTML = '<a href="tel:' . $phoneLink . '" class="phone">';

        // Override Inner HTML
        if ($innerHTMLPlacement == 'override') {
            $phoneHTML .= $innerHTML;
        }
        // Append Inner HTML
        else {
            if ($innerHTMLPlacement == 'append') {
                $phoneHTML .= $phoneNumber . $innerHTML;
            }
            // Prepend Inner HTML
            else {
                $phoneHTML .= $innerHTML . $phoneNumber;
            }
        }

        $phoneHTML .= '</a>';

        return $phoneHTML;
    }


    /**
     * Echo generated phone number anchor
     *
     * @param string $phoneNumber        - The phone number
     * @param string $innerHTML          - Inner HTML to prepend to the anchor content
     * @param string $innerHTMLPlacement - The placement of the optional inner html (prepend|append|override)
     *
     * @return string   - Echo out phone number anchor html
     */
    function thePhoneAnchor($phoneNumber = '', $innerHTML = '', $innerHTMLPlacement = 'prepend')
    {
        $phoneHTML = $phoneNumber ? phoneAnchor($phoneNumber, $innerHTML, $innerHTMLPlacement) : '';

        echo $phoneHTML;
    }


    /**
     * Generates the html for inline svg icon
     *
     * @param           $iconName   - The name of the icon
     *
     * @return string - The icon html
     */
    function svgIcon($iconName = '') {

        $svgHTML = '';

        if ($iconName) {
            $iconID = 'icon-' . $iconName;

            $svgHTML = '<svg class="icon">';
            $svgHTML .= '<use xlink:href="' . BUILD_URI . '/symbol/icons.svg#' . $iconID . '"/>';
            $svgHTML .= '</svg>';
        }

        return $svgHTML;
    }

    
    /**
     * Renders a template of a button/array of buttons 
     * 
     * @param array $data
     */
    function getButton(array $data = array())
    {
        if (array_key_exists('button', $data)) {
            $data['buttons'] = array($data['button']);
            unset($data['button']);
        }

        if ($data['buttons']) {

            $defaultAnimation = [
                'effect'    => 'fadeIn',
                'delay'     => 0,
                'interval'  => 200
            ];

            $templateData = array(
                'buttons'   => array(),
                'container' => true
            );


            foreach ($data['buttons'] as $index => $button) {

                if (isset($button['button'])) {
                    $button = $button['button'];
                }

                $button['animation']    = isset($button['animation']) ? $button['animation'] : $defaultAnimation['effect'];
                $button['delay']        = isset($button['delay']) ? $button['delay'] : ($defaultAnimation['delay'] + ($index * $defaultAnimation['interval']));
                $button['hold']         = false;

                $data['buttons'][$index] = $button;
            }


            $templateData = array_merge($templateData, $data);

            include(locate_template('layouts/components/buttons/buttons.php'));
        }
    }


    function getTitleBlock($data = array())
    {
        if (!is_null($data)) {

            $templateData = array(
                'subtitle'  => false,
                'title'     => false,
                'intro'     => false
            );

            $templateData = array_merge($templateData, $data);

            include(locate_template('layouts/components/title-block/title-block.php'));
        }
    }



    function getGallery($data = array())
    {
        if (!is_null($data)) {

            $templateData = array(
                'images'  => array()
            );

            if ( $data['gallery'] ) {

                $images = array();
                $galleryData = $data['gallery'];

                foreach ( $galleryData as $galleryDatum ) {

                    $images['title']              = $galleryDatum['title'];
                    $images['caption']            = $galleryDatum['caption'];
                    //image alt text here if needed
                    $images['alt']                = ( $galleryDatum['alt'] ) ? $galleryDatum['alt'] : $galleryDatum['title'];
                    $images['sizes']['thumbnail'] = $galleryDatum['sizes']['thumbnail'];
                    $images['sizes']['medium']    = $galleryDatum['sizes']['medium'];
                    $images['sizes']['large']     = $galleryDatum['sizes']['large'];

                    array_push($templateData['images'], $images);
                }
            }

            $templateData = array_merge($templateData, $data);

            include(locate_template('layouts/components/gallery/gallery.php'));
        }
    }

    
    function getSocialIcons($data = array())
    {

        $templateData = array(
            'circles' => false,
            'platforms' => array(
                'facebook',
                'google',
                'instagram',
                'linkedin',
                'twitter',
                'youtube'
            ),
            'social-platforms' => array()
        );

        if (!is_null($data)) {

            // Unset icons from default if set in data
            if (isset($data['platforms'])) {
                unset($templateData['platforms']);
            }

            $templateData = array_merge($templateData, $data);
        }

        for ($i = 0; $i < count($templateData['platforms']); ++$i) {
            $socialURL = get_field($templateData['platforms'][$i], 'option');

            if ($socialURL) {
                $templateData['social-platforms'][$templateData['platforms'][$i]] = $socialURL;
            }
        }

        include(locate_template('layouts/components/social-media/social-media.php'));
    }

    function getWayfinderItems($wayfinderType)
    {
        $wayfinderItems = [];

        if ($wayfinderType == 'links') {
            $wayfinderItems = get_sub_field('wayfinder_items');

        }
        elseif ($wayfinderType == 'child') {
            $wayfinderItems = get_children(array(
                'post_type'         => 'page',
                'post_status'       => 'publish',
                'posts_per_page'    => -1,
                'post_parent'       => get_the_ID(),
                'orderby'           => 'ID',
                'order'             => 'ASC'
            ));
        }

        return $wayfinderItems;
    }


    function getWayfinderFields($wayfinderType, $item)
    {
        if ($wayfinderType == 'links') {
            return [
                isset($item['wayfinder_image']) && $item['wayfinder_image'] ? $item['wayfinder_image']['sizes']['banner-bg-mobile'] : false,
                isset($item['title']) && $item['title'] ? $item['title'] : false,
                isset($item['content']) && $item['content'] ? $item['content'] : false,
                isset($item['link']) && $item['link'] ? $item['link'] : ''
            ];
        }
        elseif ($wayfinderType == 'child') {

            $img = get_field('wayfinder_image', $item->ID);

            $title = get_field('wayfinder_title', $item->ID);
            $title = $title ? : $item->post_title;

            return [
                $img ? $img['sizes']['banner-bg-mobile'] : false,
                $title,
                get_field('wayfinder_summary', $item->ID),
                get_the_permalink($item->ID)
            ];
        }

        return [];
    }

}