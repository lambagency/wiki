<?php

namespace LambAgency;

/**
 * Hides username field
 */
function registrationStyling()
{
    echo '
        <style>
            #registerform > p:first-child{
                display: none;
            }
        </style>
    ';
}
add_action('login_head', __NAMESPACE__ . '\registrationStyling');


/**
 * Remove error for username, only show error for email only.
 *
 * @param $wp_error
 * @param $sanitized_user_login
 * @param $user_email
 *
 * @return mixed
 */
function validateRegistration($wp_error, $sanitized_user_login, $user_email)
{
    if (isset($wp_error->errors['empty_username'])){
        unset($wp_error->errors['empty_username']);
    }

    if (isset($wp_error->errors['username_exists'])){
        unset($wp_error->errors['username_exists']);
    }

    return $wp_error;
}
add_filter('registration_errors', __NAMESPACE__ . '\validateRegistration', 10, 3);


/**
 * Set username as the specified email address
 */
function setUsernameAsEmail()
{
    if (isset($_POST['user_login']) && isset($_POST['user_email']) && !empty($_POST['user_email'])){
        $_POST['user_login'] = $_POST['user_email'];
    }
};
add_action('login_form_register', __NAMESPACE__ . '\setUsernameAsEmail');