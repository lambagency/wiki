<?php

/**
 * Defines Widget areas
 */
function default_widgets_init()
{
    register_sidebar(array(
        'name'          => __('Sidebar Blog Post'),
        'id'            => 'sidebar-blog-post-widget',
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget'  => "</li>",
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ));

    register_sidebar(array(
        'name'          => __('Sidebar Blog List', 'default'),
        'id'            => 'sidebar-blog-list-widget',
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget'  => "</li>",
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ));
}
//add_action('widgets_init', 'default_widgets_init');