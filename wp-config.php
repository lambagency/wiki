<?php

/**
 * NOTE: ensure you have run composer to install necessary packages
 */

require ABSPATH . 'vendor/autoload.php';

// set up environment variables
$dotenv = new Dotenv\Dotenv(ABSPATH);
$dotenv->load();
$dotenv->required(['DB_HOST', 'DB_NAME', 'DB_USER', 'DB_PASSWORD'])->notEmpty();



// DB Config
//---------------------------------

define('DB_HOST', getenv('DB_HOST'));
define('DB_NAME', getenv('DB_NAME'));
define('DB_USER', getenv('DB_USER'));
define('DB_PASSWORD', getenv('DB_PASSWORD'));
define('DEV_ENVIRONMENT', (bool)(int)getenv('DEV_ENVIRONMENT'));
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');
define('DB_PREFIX', getenv('DB_PREFIX'));

$table_prefix = DB_PREFIX;



// Debug Config
//---------------------------------

define('WP_DEBUG', (bool)(int)getenv('WP_DEBUG'));
define('WP_DEBUG_DISPLAY', (bool)(int)getenv('WP_DEBUG_DISPLAY'));
define('WP_DEBUG_LOG', (bool)(int)getenv('WP_DEBUG_LOG'));


// set up outputting of nice displaying of errors - https://github.com/filp/whoops
if (class_exists('\Whoops\Run') && defined('DEV_ENVIRONMENT') && DEV_ENVIRONMENT) {
    $whoops = new \Whoops\Run;
    $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
    $whoops->register();
}



// Logging
//---------------------------------

define('ERROR_EMAIL', getenv('ERROR_EMAIL'));
define('LOG_FILE_NAME', getenv('LOG_FILE_NAME'));
define('LOG_DB', (bool)(int)getenv('LOG_DB'));
define('LOG_DB_NAME', getenv('LOG_DB_NAME'));



define('AUTH_KEY',         'D&At1=O(2>)y`xZzJjs42r2LvU;+)%G->#?$rHiP96o*nnnKQNqtdUhT$EZYz:x4');
define('SECURE_AUTH_KEY',  '-+z[rR)KEgaC-LL+J$dVyn dW!~{)=o|01Px2D-5#J|&>@~DL1Daab6g5}|&, -J');
define('LOGGED_IN_KEY',    '-RBGrY{wk/s?jc]/g&RIEt0<%FWpl.1$pqvoGc54q-OHjm{N+@lgNXLP}]=?m^i^');
define('NONCE_KEY',        'qQk|-V|mZa=I!)3%Jp4-,Xv-n9+QH,<JqYU|L`&&/8ZpHy|y&Z@LiqH?`[0KZwwN');
define('AUTH_SALT',        '1A2?}-ZiJ$e5`Ag+]c]O+3}_`*KVJmn-G^^`TEUU:SM4y(gy|^WzMIFMu33Jn c?');
define('SECURE_AUTH_SALT', 'rW;c u>xjA0t-dCIc&P$sk{+:-p6U!&-JiTS:~cg,zy[(k|?[=9}Ak<&9b^+V47`');
define('LOGGED_IN_SALT',   'j`!8kc>K}~KMlq_(QR u9y+&H4+3-9n--cVnJd4Q1(.4>PC>0>Co^]t|A&O3e4mA');
define('NONCE_SALT',       'H8U$hxZax@I uEa-L/+{{LJ@@11fjYg7|am=/5-+#Qv7$*`]MT9Tok9v/|O!sMl0');


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');



define('DISALLOW_FILE_EDIT', true);
