var tasksPath = './gulp/tasks/';

// Load Gulp
var gulp = require('gulp');

// Load config
var config = require(tasksPath + 'load-config')();

// Load npm modules
var plugins = require('gulp-load-plugins')();
plugins.browserslist = require('browserslist')(config.browsers);
plugins.autoprefixer = require('autoprefixer');
plugins.mqpacker = require('css-mqpacker');
plugins.cssnano = require('cssnano');


// Default
require(tasksPath + 'default.js')(gulp, plugins, config);

// Production
require(tasksPath + 'production.js')(gulp, plugins, config);

// Watch
require(tasksPath + 'watch.js')(gulp, plugins, config);

// Icons
require(tasksPath + 'icons.js')(gulp, plugins, config);


// System JS concatenation
require(tasksPath + 'system-js')(gulp, plugins, config);

// Lamb JS concatenation
require(tasksPath + 'lamb-js')(gulp, plugins, config);

// JS concatenation
require(tasksPath + 'js')(gulp, plugins, config);

// JS Min
require(tasksPath + 'min-js')(gulp, plugins, config);

// SCSS compilation
require(tasksPath + 'scss.js')(gulp, plugins, config);

// SCSS Min
require(tasksPath + 'min-scss.js')(gulp, plugins, config);

// Supported Browsers
require(tasksPath + 'browsers.js')(gulp, plugins, config);
