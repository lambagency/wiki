module.exports = function(gulp, plugins, config) {

    /**
     * Default task producing dev assets
     */
    gulp.task('default', function() {
        return gulp.start(['js', 'scss', 'admin-scss']);
    });
};
