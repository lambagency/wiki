module.exports = function(gulp, plugins, config) {

    /**
     * Create css files from sass.
     * Includes auto-prefix processing for cross-browser support.
     */
    gulp.task('scss', function() {

        var processors = [
            plugins.autoprefixer(plugins.browserslist)
        ];

        return gulp.src(config.paths.scss.lamb.src)
            .pipe(plugins.expectFile(config.paths.scss.lamb.src))
            .pipe(plugins.sass()
                .on('error', plugins.sass.logError)
            )
            .pipe(plugins.concat(config.paths.scss.lamb.name))
            .pipe(plugins.postcss(processors))
            .pipe(gulp.dest(config.paths.scss.lamb.dest))
            .pipe(plugins.connect.reload());
    });

    gulp.task('admin-scss', function() {
        var processors = [
            plugins.autoprefixer(plugins.browserslist)
        ];

        return gulp.src(config.paths.scss.admin.src)
            .pipe(plugins.expectFile(config.paths.scss.admin.src))
            .pipe(plugins.sass()
                .on('error', plugins.sass.logError)
            )
            .pipe(plugins.postcss(processors))
            .pipe(gulp.dest(config.paths.scss.admin.dest));
    });
};
