module.exports = function(gulp, plugins, config) {

    var svgSpriteConfig = {
        svg : {
            namespaceClassnames: false
        },
        mode: {
            symbol: {
                sprite: config.paths.icons.sprite.name
            }
        }
    };


    gulp.task('clean-icons', function() {
        return gulp.src(config.paths.icons.dest)
            .pipe(plugins.clean());
    });
    
    gulp.task('icon-minify', ['clean-icons'], function() {
        return gulp.src(config.paths.icons.src)
            .pipe(plugins.rename(function(path) {
                // Lowercase filename
                path.basename = path.basename.toLowerCase();
                // Hyphenate filename spaces
                path.basename = path.basename.replace(/\s+/g, '-');
                // Remove icon prefixes from illustrator
                path.basename = path.basename.replace(/icons?[-_]/g, '');
            }))
            .pipe(plugins.cheerio({
                run: function ($, file) {
                    var $svg = $('svg');

                    $svg.addClass('icon');

                    // Remove style and fill attributes to allow full CSS control
                    $('[fill]').removeAttr('fill');
                    $('[style]').removeAttr('style');
                },
                parserOptions: { xmlMode: true }
            }))

            .pipe(plugins.svgmin({
                plugins: [{
                    removeDoctype: true
                }, {
                    removeComments: true
                }, {
                    cleanupNumericValues: {
                        floatPrecision: 1
                    }
                }, {
                    convertColors: {
                        names2hex: false,
                        rgb2hex: false
                    }
                }]
            }))
            .pipe(gulp.dest(config.paths.icons.dest));
    });


    gulp.task('icon-sprite', ['icon-minify'], function() {
        return gulp.src(config.paths.icons.dest + '/**/*.svg')
            .pipe(plugins.rename(function(path) {
                // Prefix filename with 'icon-'
                path.basename = 'icon-' + path.basename;
            }))
            .pipe(plugins.svgSprite(svgSpriteConfig))
            .pipe(gulp.dest(config.paths.icons.sprite.dest));
    });

    gulp.task('icon-font', ['icon-minify'], function() {
        var runTimestamp = Math.round(Date.now()/1000);

        return gulp.src(config.paths.icons.dest + '/**/*.svg')
            .pipe(plugins.iconfont({
                fontName: config.paths.icons.font.name, // required
                formats: ['ttf', 'eot', 'woff', 'svg'], // default, 'woff2' and 'svg' are available
                timestamp: runTimestamp // recommended to get consistent builds when watching files
            }))
            .on('glyphs', function(glyphs) {
                gulp.src(config.paths.icons.font.scss + 'template.txt')
                    .pipe(plugins.consolidate('lodash', {
                        glyphs: glyphs,
                        fontName: config.paths.icons.font.name,
                        fontPath: '../fonts/' + config.paths.icons.font.name + '/', // set path to font (from your CSS file if relative)
                        className: 'licon' // set class name in your CSS
                    }))
                    .pipe(plugins.rename(config.paths.icons.font.name + '.scss'))
                    .pipe(gulp.dest(config.paths.icons.font.scss)); // set path to export your CSS
            })
            .pipe(gulp.dest(config.paths.icons.font.dest + config.paths.icons.font.name));
    });


    gulp.task('icons', ['icon-sprite', 'icon-font'], function() {});
};
