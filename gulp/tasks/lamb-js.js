module.exports = function(gulp, plugins, config) {

    /**
     * Create single js file from lamb js files
     */
    gulp.task('lamb-js', function() {
    
        return gulp.src(config.paths.js.lamb.src)
            .pipe(plugins.concat(config.paths.js.lamb.name))
            .pipe(gulp.dest(config.paths.js.lamb.dest));
    });
};
