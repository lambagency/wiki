module.exports = function(gulp, plugins, config) {

    /**
     * Create single js file from lib js files
     */
    gulp.task('system-js', function() {
    
        return gulp.src(config.paths.js.lib.src)
            .pipe(plugins.expectFile(config.paths.js.lib.src))
            .pipe(plugins.concat(config.paths.js.lib.name))
            .pipe(gulp.dest(config.paths.js.lib.dest));
    });
};
