module.exports = function(gulp, plugins, config) {
    
    /**
     * Create production files.
     */
    gulp.task('production', ['icons'], function() {
        return gulp.start(['min-js', 'min-scss']);
    });
};
