module.exports = function(gulp, plugins, config) {
    /**
     * Create minified scss for production environment
     */
    gulp.task('min-scss', ['scss', 'admin-scss'], function() {

        var processors = [
            plugins.mqpacker(),
            plugins.cssnano()
        ];

        var scssFiles = [
            config.paths.scss.lamb.dest + config.paths.scss.lamb.name,
            config.paths.scss.admin.dest + config.paths.scss.admin.name
        ];

        return gulp.src(scssFiles)
            .pipe(plugins.postcss(processors))
            .pipe(plugins.rename({ extname: '.min.css' }))
            .pipe(gulp.dest(config.paths.scss.lamb.dest));
    });
};
