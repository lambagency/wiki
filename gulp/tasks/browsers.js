module.exports = function(gulp, plugins, config) {

    /**
     * Log a list of browsers being supported.
     * Supported browsers are defined in gulp/config.json with key "browsers"
     * Browser list is used to determine which auto-prefixers are used.
     */
    gulp.task('browsers', function() {
        console.log(plugins.browserslist);
        return true;
    });
};
