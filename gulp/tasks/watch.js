module.exports = function(gulp, plugins, config) {

    var cors = function (req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Headers', '*');
        next();
    };

    gulp.task('watch', ['default'], function() {

        plugins.watch(config.paths.watch.scss, function (files) {
            return gulp.start(['scss']);
        });

        plugins.watch(config.paths.watch.js, function (files) {
            return gulp.start(['js']);
        });

        plugins.connect.server({
            root: config.paths.watch.livereload.path,
            port: 8000,
            livereload: true,
            debug: true,
            middleware: function () {
                return [cors];
            }
        });

    });
};
