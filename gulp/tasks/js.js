module.exports = function(gulp, plugins, config) {

    /**
     * Create development js file
     */
    gulp.task('js', ['system-js', 'lamb-js'], function() {

        return gulp.src(config.paths.js.src)
            .pipe(plugins.concat(config.paths.js.name))
            .pipe(gulp.dest(config.paths.js.dest));
    });
};
