module.exports = function(gulp, plugins, config) {

    /**
     * Create minified js for production environment
     */
    gulp.task('min-js', ['js'], function() {

        return gulp.src(config.paths.js.dest + 'production.js')
            .pipe(plugins.uglify())
            .pipe(plugins.rename({ extname: '.min.js' }))
            .pipe(gulp.dest(config.paths.js.dest));
    });
};
