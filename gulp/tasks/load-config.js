/**
 * Load configuration file and replace template and build directories with
 * theme variable defined in package json.
 */
module.exports = function() {

    // Load package JSON to access variables
    var packageJSON = require('../../package.json');

    // Load config JSON
    var rawConfig = require('../config.json');

    rawConfig = JSON.stringify(rawConfig);
    // Replace all occurrences of {{templateDir}} with template directory relative url
    rawConfig = rawConfig.replace(/{{templateDir}}/g, packageJSON.templateDir);

    // Replace all occurrences of {{buildDir}} with build directory relative url
    rawConfig = rawConfig.replace(/{{buildDir}}/g, packageJSON.templateDir + 'interface/build/');

    return JSON.parse(rawConfig);
};
